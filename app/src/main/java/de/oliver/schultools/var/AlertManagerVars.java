package de.oliver.schultools.var;

public class AlertManagerVars {
    public static final String VP_TODAY_VAR_1 = "Notify_VP_Today_1";
    public static final String VP_TODAY_VAR_2 = "Notify_VP_Today_2";
    public static final String VP_TODAY_VAR_3 = "Notify_VP_Today_3";

    public static final String VP_TOMORROW_VAR_1 = "Notify_VP_Tomorrow_1";
    public static final String VP_TOMORROW_VAR_2 = "Notify_VP_Tomorrow_2";
    public static final String VP_TOMORROW_VAR_3 = "Notify_VP_Tomorrow_3";

    public static final String VP_MONDAY_VAR_1 = "Notify_VP_Monday_1";
    public static final String VP_MONDAY_VAR_2 = "Notify_VP_Monday_2";
    public static final String VP_MONDAY_VAR_3 = "Notify_VP_Monday_3";

    public static final String HW_ALL_VAR_1 = "HW_ALL_VAR_1";
    public static final String HW_ALL_VAR_2 = "HW_ALL_VAR_2";
    public static final String HW_ALL_VAR_3 = "HW_ALL_VAR_3";

    public static final String TEST_NV_VAR_1 = "TEST_TEST_NV_1";
}
