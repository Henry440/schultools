package de.oliver.schultools.var.user_vars;

public class GeneralVars {
    public static boolean isSchool;
    public static boolean isFree;

    public static boolean getNotificationAll;
    public static boolean getNotificationVP;
    public static boolean getNotificationLessonStart;
    public static boolean getNotificationHomeWorkAlert;

    public static boolean allowUsedInternet;
    public static boolean hasInternetConnection;

    public static boolean vPavivabelTD;
    public static boolean vPavivabelTM;

    public static boolean app_is_running = false;

    public static final String URL_SCHULWEBSITE = "https://ags-erfurt.de/de/";
    public static final String URL_MOODLE = "https://moodle.ags-erfurt.de/login/index.php";
}
