package de.oliver.schultools.var;

public class NotificationVars {
    public static final String CHANNEL_VP_ID = "channelVP";
    public static final String CHANNEL_LS_ID = "channelLS";
    public static final String CHANNEL_HW_ID = "channelHW";


    public static final String CHANNEL_TEST_ID = "channelTEST";
}
