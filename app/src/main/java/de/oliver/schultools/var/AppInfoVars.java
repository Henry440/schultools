package de.oliver.schultools.var;

public class AppInfoVars {

    public static final String PROGRAMMER = "Oliver Audehm";
    public static final String PROJECT_START = "30.11.2018 / 12.00 Uhr";
    public static final String LAST_CHANCES = "08.06.2019 / 15.05 Uhr";
    public static final String[] APP_HINT = {"Die ist eine Inoffizielle App der Andreas-Gorden-Schule", "Die App Steht in keinem Zusammenhang mit der oben genannten Schule", "Verwendung besteht auf eigene Gefahr, Ich hafte für keine Entstandenen Schäden und Kosten die die App verursacht hat."};
    public static final String[] KONTAKT_EMAIL = {"E-Mail : oliver.audehm@gmail.com", "E-Mail : 440henry@gmail.com"};
    public static final String[] APP_FUNKTIONEN = {"Stundenanzeige", "Stundenplananzeige", "Download Vertretungsplan","Benachichtigung zum Vertretungsplan", "Eintragen der Hausaufgaben", "Links auf Schulwebsite", "Anzeige Praktikumszeiten"};
    public static final String[] UPDATE_NOTES = {"Fehler entfernt Appcrash Download Vertretungsplan", "Implementierung Crashlytics für Fehleranalyse"};
    public static final String[] APP_NEXT_FUNCTIONS = {"Praktikums Startseite", "Erweitern Startseite Schule"};
    public static final String APP_BEMERKUNG = "Die App läuft noch nicht sabiel und kann häufig Abstürzen. Sie läuft ab Android 7.0 da einige Funktionen erst aber der Version Verfügbar sind.";

}
