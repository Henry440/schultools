package de.oliver.schultools.var.user_vars;

public class ServiceVars {
    public static boolean ServicesRunning = true;
    public static boolean ServicesVP = true;
    public static boolean ServicesHA = false;
    public static long RefreshTime = 1000 * 60 * 20;

    public static final String TITLE_TODAY = "Vertretungsplan Heute";
    public static final String CONTENT_TODAY = "Der Vertretungsplan von heute wurde entfernt";
    public static final int MSG_ID_TODAY = 1;

    public static final String TITLE_TOMORROW = "Vertretungsplan Morgen";
    public static final String CONTENT_TOMORROW = "Der Vertretungsplan für morgen wurde hinzugefügt";
    public static final int MSG_ID_TOMORROW = 2;

    public static final String TITLE_MONDAY = "Vertretungsplan Montag";
    public static final String CONTENT_MONDAY = "Der Vertretungsplan für Montag wurde hinzugefügt";
    public static final int MSG_ID_MONDAY = 2;
}
