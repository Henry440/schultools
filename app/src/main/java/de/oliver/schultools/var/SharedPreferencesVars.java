package de.oliver.schultools.var;

public class SharedPreferencesVars {
    public static final String spFirstStart = "keySPFirstStart"; //Contains the first App start

    public static final String spAppDataKey = "keySPAppData"; //Load vars

    public static final String spBoolIsSchool = "keySPIsSchool";
    public static final String spBoolIsFree = "keySPIsFree";

    public static final String spBoolGetNotifyAll = "keySPgetnotall";
    public static final String spBoolgetNotifyVP = "keySPgetnotvp";
    public static final String spBoolGetNotifyLessonStart = "keySPgetnotls";
    public static final String spBoolGetNotifyHomeWorkAlert = "keySPgetnothwa";

    public static final String spBoolUsedInet = "keySPuseinet";
    public static final String spBoolHasInetCon = "keySPhasinet";

    public static final String spBoolTDVP = "keyVPTD";
    public static final String spBoolTMVP = "keyVPTM";
}
