package de.oliver.schultools.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.models.PraktikumszeitenModel;
import de.oliver.schultools.models.StundenplanModel;


public class PraktikumTimesDB extends SQLiteAssetHelper {

    private static PraktikumTimesDB INSTANCE;

    private static final String DB_NAME = "worktimes.db";
    private static final int    DB_VERSION = 1;

    public static final String TABLE_NAME               = "worktimes";
    public static final String COLUMN_ID                = "ID";
    public static final String COLUMN_NAME              = "Name";
    public static final String COLUMN_FROM              = "Zeit_Von";
    public static final String COLUMN_TO                = "Zeit_Bis";
    public static final String COLUMN_Position          = "Position";


    public PraktikumTimesDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static PraktikumTimesDB getInstance(final Context context){
        if(INSTANCE == null){
            INSTANCE = new PraktikumTimesDB(context);
        }
        return INSTANCE;
    }

    public PraktikumszeitenModel creatWork(final PraktikumszeitenModel model){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, model.getName());
        values.put(COLUMN_FROM, model.getZeit_von());
        values.put(COLUMN_TO, model.getZeit_bis());
        values.put(COLUMN_Position, model.getPosition());

        long newID = db.insert(TABLE_NAME, null, values);
        db.close();
        return readWork(newID);
    }

    public PraktikumszeitenModel readWork(final long id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{COLUMN_ID, COLUMN_NAME, COLUMN_FROM, COLUMN_TO, COLUMN_Position},
                COLUMN_ID + " = ?", new String[]{String .valueOf(id)}, null,null,null);

        PraktikumszeitenModel model = null;

        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            model = new PraktikumszeitenModel(
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_FROM)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_TO)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_Position)));

            model.setID(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        }

        db.close();
        return model;
    }

    public List<PraktikumszeitenModel> getAllWork(){
        List<PraktikumszeitenModel> allWork = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM "+ TABLE_NAME, null);

        if(cursor.moveToFirst()){
            do{
                PraktikumszeitenModel model = readWork(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));

                if(model != null){
                    allWork.add(model);
                }
            }while (cursor.moveToNext());
        }
        db.close();
        return allWork;
    }

    public PraktikumszeitenModel updateWork(final PraktikumszeitenModel model){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_NAME, model.getName());
        values.put(COLUMN_FROM, model.getZeit_von());
        values.put(COLUMN_TO, model.getZeit_bis());
        values.put(COLUMN_Position, model.getPosition());

        db.update(TABLE_NAME, values, COLUMN_ID + " = ?", new String[]{String.valueOf(model.getID())});

        db.close();

        return this.readWork(model.getID());
    }

    public void deleteWork(final PraktikumszeitenModel model){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + " = ?", new String[]{String.valueOf(model.getID())});
        db.close();
    }

    public void deletaAllWork(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME);
        db.close();
    }
}
