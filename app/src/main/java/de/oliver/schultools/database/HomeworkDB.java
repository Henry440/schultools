package de.oliver.schultools.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.adapter.HausaufgabenAdapter;
import de.oliver.schultools.models.HausaufgabenModel;

public class HomeworkDB extends SQLiteAssetHelper {

    private static HomeworkDB INSTANCE;

    private static final String DB_NAME = "homework.db";
    private static final int    DB_VERSION = 1;

    public static final String TABLE_NAME               = "homework";
    public static final String COLUMN_ID                = "ID";
    public static final String COLUMN_HAUSAUFGABENNAME  = "HausaufgabenName";
    public static final String COLUMN_FACH              = "Fach";
    public static final String COLUMN_STUNDE            = "Stunde";
    public static final String COLUMN_TAG               = "Tag";
    public static final String COLUMN_WO                = "Wo";
    public static final String COLUMN_AUFGABE           = "Aufgabe";
    public static final String COLUMN_BEWERTUNG         = "Bewertung";
    public static final String COLUMN_ALARM             = "Alarm";
    public static final String COLUMN_ERLEDIGT          = "Erledigt";
    public static final String COLUMN_BEMERKUNG         = "Bemerkung";

    public HomeworkDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static HomeworkDB getInstance(final Context context){
        if(INSTANCE == null){
            INSTANCE = new HomeworkDB(context);
        }

        return INSTANCE;
    }


    public HausaufgabenModel createHomework(final HausaufgabenModel hausaufgabenModel){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_HAUSAUFGABENNAME, hausaufgabenModel.getHausaufagenName());
        values.put(COLUMN_FACH, hausaufgabenModel.getFach());
        values.put(COLUMN_STUNDE, hausaufgabenModel.getStunde());
        values.put(COLUMN_TAG, hausaufgabenModel.getTag());
        values.put(COLUMN_WO, hausaufgabenModel.getWo());
        values.put(COLUMN_AUFGABE, hausaufgabenModel.getAufgabe());
        values.put(COLUMN_BEWERTUNG, hausaufgabenModel.getBewertung());
        values.put(COLUMN_ALARM, hausaufgabenModel.getAlarm());
        values.put(COLUMN_ERLEDIGT, hausaufgabenModel.getErledigt());
        values.put(COLUMN_BEMERKUNG, hausaufgabenModel.getBemerkung());


        long newID = db.insert(TABLE_NAME, null, values);
        db.close();
        return readHomework(newID);
    }

    public HausaufgabenModel updateHomework(final HausaufgabenModel hausaufgabenModel){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_HAUSAUFGABENNAME, hausaufgabenModel.getHausaufagenName());
        values.put(COLUMN_FACH, hausaufgabenModel.getFach());
        values.put(COLUMN_STUNDE, hausaufgabenModel.getStunde());
        values.put(COLUMN_TAG, hausaufgabenModel.getTag());
        values.put(COLUMN_WO, hausaufgabenModel.getWo());
        values.put(COLUMN_AUFGABE, hausaufgabenModel.getAufgabe());
        values.put(COLUMN_BEWERTUNG, hausaufgabenModel.getBewertung());
        values.put(COLUMN_ALARM, hausaufgabenModel.getAlarm());
        values.put(COLUMN_ERLEDIGT, hausaufgabenModel.getErledigt());
        values.put(COLUMN_BEMERKUNG, hausaufgabenModel.getBemerkung());
        db.update(TABLE_NAME, values, COLUMN_ID + " = ?", new String[]{String.valueOf(hausaufgabenModel.getID())});

        db.close();
        return this.readHomework(hausaufgabenModel.getID());
    }

    public void deleteHomework(final HausaufgabenModel hausaufgabenModel){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + " = ?", new String[]{String.valueOf(hausaufgabenModel.getID())});
        db.close();
    }

    public HausaufgabenModel readHomework(final long id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{COLUMN_ID, COLUMN_HAUSAUFGABENNAME, COLUMN_FACH, COLUMN_STUNDE, COLUMN_TAG, COLUMN_WO, COLUMN_AUFGABE, COLUMN_BEWERTUNG, COLUMN_ALARM, COLUMN_ERLEDIGT, COLUMN_BEMERKUNG},
                COLUMN_ID + " = ?", new String[]{String .valueOf(id)}, null,null,null);

        HausaufgabenModel hausaufgabe = null;
        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            hausaufgabe = new HausaufgabenModel(
                    cursor.getString(cursor.getColumnIndex(COLUMN_HAUSAUFGABENNAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_FACH)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_STUNDE)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_TAG)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_WO)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_AUFGABE)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_BEWERTUNG)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_ALARM)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_ERLEDIGT)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_BEMERKUNG)));


            hausaufgabe.setID(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        }
        cursor.close();
        db.close();
        return hausaufgabe;
    }

    public List<HausaufgabenModel> getAllHomework(){
        List<HausaufgabenModel> allStunden = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM "+ TABLE_NAME, null);

        if(cursor.moveToFirst()){
            do{
                HausaufgabenModel stunde = readHomework(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));

                if(stunde != null){
                    allStunden.add(stunde);
                    System.out.println("YES : " + stunde.getHausaufagenName());
                }
            }while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return allStunden;
    }
}
