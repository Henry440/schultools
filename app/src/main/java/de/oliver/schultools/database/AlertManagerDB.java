package de.oliver.schultools.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.models.AlertManagerModel;

public class AlertManagerDB extends SQLiteAssetHelper {

    private static AlertManagerDB INSTANCE;

    private static final String DB_NAME     = "alertmanager.db";
    private static final int    DB_VERSION  = 1;

    public static final String TABLE_NAME               = "alertmanager";
    public static final String COLUMN_ID                = "id";
    public static final String COLUMN_CALLERNAME        = "callername";
    public static final String COLUMN_LASTCALLDAY       = "lastcallday";
    public static final String COLUMN_CALLSATDAY        = "callsatday";
    public static final String COLUMN_POSIBLECALLS      = "posiblecalls";

    public AlertManagerDB(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static AlertManagerDB getInstance(final Context context){
        if(INSTANCE == null){
            INSTANCE = new AlertManagerDB(context);
        }
        return INSTANCE;
    }

    public AlertManagerModel creatAlertModel(final AlertManagerModel model){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CALLERNAME, model.getCallername());
        values.put(COLUMN_LASTCALLDAY, model.getLastcallday());
        values.put(COLUMN_CALLSATDAY, model.getCalls_at_day());
        values.put(COLUMN_POSIBLECALLS, model.getPosible_calls());

        long newID = db.insert(TABLE_NAME, null, values);
        db.close();
        return readAlertModel(newID);
    }

    public AlertManagerModel readAlertModel(final long id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{COLUMN_ID, COLUMN_CALLERNAME, COLUMN_LASTCALLDAY, COLUMN_CALLSATDAY, COLUMN_POSIBLECALLS},
                COLUMN_ID + " = ?", new String[]{String .valueOf(id)}, null,null,null);

        AlertManagerModel model = null;

        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            model = new AlertManagerModel(
                    cursor.getString(cursor.getColumnIndex(COLUMN_CALLERNAME)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_LASTCALLDAY)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_CALLSATDAY)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_POSIBLECALLS)));

            model.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        }
        cursor.close();
        db.close();
        return model;
    }

    public List<AlertManagerModel> getAllAlertModels(){
        List<AlertManagerModel> allModel = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM "+ TABLE_NAME, null);

        if(cursor.moveToFirst()){
            do{
                AlertManagerModel model = readAlertModel(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));

                if(model != null){
                    allModel.add(model);
                }
            }while (cursor.moveToNext());
        }
        db.close();
        return allModel;
    }

    public AlertManagerModel updateAlertModel(final AlertManagerModel model){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_CALLERNAME, model.getCallername());
        values.put(COLUMN_LASTCALLDAY, model.getLastcallday());
        values.put(COLUMN_CALLSATDAY, model.getCalls_at_day());
        values.put(COLUMN_POSIBLECALLS, model.getPosible_calls());

        db.update(TABLE_NAME, values, COLUMN_ID + " = ?", new String[]{String.valueOf(model.getId())});

        db.close();

        return this.readAlertModel(model.getId());
    }

    public void deleteAlertModel(final AlertManagerModel model){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + " = ?", new String[]{String.valueOf(model.getId())});
        db.close();
    }

    public void deletaAllAlertModels(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME);
        db.close();
    }
}
