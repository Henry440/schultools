package de.oliver.schultools.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.models.StundenplanModel;

public class TimetableDB extends SQLiteAssetHelper {
    private static TimetableDB INSTANCE;

    private static final String DB_NAME = "stundenplan.db";
    private static final int    DB_VERSION = 1;

    public static final String  TABLE_NAME = "timetable";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_STUNDE = "stunde";
    public static final String COLUMN_ZEIT_VON = "zeit_von";
    public static final String COLUMN_ZEIT_BIS = "zeit_bis";
    public static final String COLUMN_FACH = "fach";
    public static final String COLUMN_RAUM = "raum";
    public static final String COLUMN_TAG = "tag";

    private TimetableDB(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static TimetableDB getInstance(final Context context){
        if(INSTANCE == null){
            INSTANCE = new TimetableDB(context);
        }

        return INSTANCE;
    }

    public Cursor getCursor(){
        return this.getReadableDatabase().rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

    //Edit Database Functions

    public StundenplanModel creatStunde(final StundenplanModel stunde){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_STUNDE, stunde.getStunde());
        values.put(COLUMN_ZEIT_VON, stunde.getZeit_Von());
        values.put(COLUMN_ZEIT_BIS, stunde.getZeit_Bis());
        values.put(COLUMN_FACH, stunde.getFach());
        values.put(COLUMN_RAUM, stunde.getRaum());
        values.put(COLUMN_TAG, stunde.getTag());

        long newID = db.insert(TABLE_NAME, null, values);
        db.close();
        return readStunde(newID);
    }

    public StundenplanModel readStunde(final long id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{COLUMN_ID, COLUMN_STUNDE, COLUMN_ZEIT_VON, COLUMN_ZEIT_BIS, COLUMN_FACH, COLUMN_RAUM, COLUMN_TAG},
                                COLUMN_ID + " = ?", new String[]{String .valueOf(id)}, null,null,null);

        StundenplanModel stunde = null;

        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            stunde = new StundenplanModel(
                    cursor.getInt(cursor.getColumnIndex(COLUMN_STUNDE)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_ZEIT_VON)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_ZEIT_BIS)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_FACH)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_RAUM)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_TAG)));

            stunde.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        }

        db.close();
        return stunde;
    }

    public List<StundenplanModel> getAllStunden(){
        List<StundenplanModel> allStunden = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM "+ TABLE_NAME, null);

        if(cursor.moveToFirst()){
            do{
                StundenplanModel stunde = readStunde(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));

                if(stunde != null){
                    allStunden.add(stunde);
                }
            }while (cursor.moveToNext());
        }
        db.close();
        return allStunden;
    }

    public StundenplanModel updateStunde(final StundenplanModel stunde){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_STUNDE, stunde.getStunde());
        values.put(COLUMN_ZEIT_VON, stunde.getZeit_Von());
        values.put(COLUMN_ZEIT_BIS, stunde.getZeit_Bis());
        values.put(COLUMN_FACH, stunde.getFach());
        values.put(COLUMN_RAUM, stunde.getRaum());
        values.put(COLUMN_TAG, stunde.getTag());

        db.update(TABLE_NAME, values, COLUMN_ID + " = ?", new String[]{String.valueOf(stunde.getId())});

        db.close();

        return this.readStunde(stunde.getId());
    }

    public void deleteStunde(final StundenplanModel stunde){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_ID + " = ?", new String[]{String.valueOf(stunde.getId())});
        db.close();
    }

    public void deletaAllStunden(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME);
        db.close();
    }

}
