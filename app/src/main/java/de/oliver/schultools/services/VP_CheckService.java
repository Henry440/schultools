package de.oliver.schultools.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import static de.oliver.schultools.functions.AlertManager.alertAllowed;
import static de.oliver.schultools.functions.DownloadVP.*;
import static de.oliver.schultools.functions.ManageSharedPreferences.*;
import static de.oliver.schultools.functions.NotificationHandling.sendNotification;
import static de.oliver.schultools.var.AlertManagerVars.*;
import static de.oliver.schultools.var.user_vars.ServiceVars.*;

public class VP_CheckService extends Service {

    public static VP_CheckService vp_checkService;

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        vp_checkService = this;
        loadSH(getApplicationContext());

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;

                while(ServicesVP){
                    i++;
                    System.out.println("YES : Background Work : Count : " + i);
                    try {
                        checkVerfuegbar(getApplicationContext());
                    }catch (Exception e){
                        e.printStackTrace();
                        System.out.println("ERROR FIXED???");
                    }
                    int status = 0;

                    status = getStatus();

                    System.out.println("YES : Status : " + status + " Lauf : " + i);

                    if(status == 0){

                    }else if (status == 1 && alertAllowed(VP_TODAY_VAR_1, getApplicationContext())){
                        sendNotification(getApplicationContext(),1, TITLE_TODAY, CONTENT_TODAY, MSG_ID_TODAY);
                    }else if(status == 2 && alertAllowed(VP_TOMORROW_VAR_1, getApplicationContext())){
                        sendNotification(getApplicationContext(),1, TITLE_TOMORROW, CONTENT_TOMORROW, MSG_ID_TOMORROW);
                    }else if (status == 3 && alertAllowed(VP_MONDAY_VAR_1, getApplicationContext())){
                        sendNotification(getApplicationContext(),1, TITLE_MONDAY, CONTENT_MONDAY, MSG_ID_MONDAY);
                    }else if(status == 4 && (alertAllowed(VP_TODAY_VAR_2, getApplicationContext()) || alertAllowed(VP_TOMORROW_VAR_2, getApplicationContext()))){
                        sendNotification(getApplicationContext(),1, TITLE_TODAY, CONTENT_TODAY, MSG_ID_TODAY);
                        sendNotification(getApplicationContext(),1, TITLE_TOMORROW, CONTENT_TOMORROW, MSG_ID_TOMORROW);
                    }else if (status == 5 && (alertAllowed(VP_TODAY_VAR_2, getApplicationContext()) || alertAllowed(VP_MONDAY_VAR_2, getApplicationContext()))){
                        sendNotification(getApplicationContext(),1, TITLE_TODAY, CONTENT_TODAY, MSG_ID_TODAY);
                        sendNotification(getApplicationContext(),1, TITLE_MONDAY, CONTENT_MONDAY, MSG_ID_MONDAY);
                    }else{
                        if(alertAllowed(TEST_NV_VAR_1, getApplicationContext())){
                            sendNotification(getApplicationContext(), 0, "No","No Case", 3);
                        }
                    }

                    try {
                        Thread.sleep(RefreshTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();

        return super.onStartCommand(intent, flags, startId);
    }

    public static  VP_CheckService getInstance(){
        return vp_checkService;
    }

    private int getStatus() {//TODO Check function
        loadSH(getApplicationContext());
        int ret = 0;
        if(chanche_in_vp[0][1]){
            ret = 1;
        }
        if(chanche_in_vp[1][0]){
            if(ret == 1){
                ret = 4;
            }else{
                ret = 2;
            }
        }

        if(chanche_in_vp[2][0]){
            if(ret == 1){
                ret = 0;
            }else{
                ret = 0;
            }
        }
        //mFirebaseAnalytics.logEvent(String.valueOf(ret),null);
        return ret;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
