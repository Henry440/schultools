package de.oliver.schultools.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.activity.MainActivity;
import de.oliver.schultools.database.HomeworkDB;
import de.oliver.schultools.models.HausaufgabenModel;

import static de.oliver.schultools.functions.AlertManager.alertAllowed;
import static de.oliver.schultools.functions.ManageSharedPreferences.loadSH;
import static de.oliver.schultools.functions.NotificationHandling.sendNotification;
import static de.oliver.schultools.var.AlertManagerVars.HW_ALL_VAR_1;

public class HA_AlertService extends Service {

    public static HA_AlertService ha_alertService;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ha_alertService = this;
        loadSH(getApplicationContext());
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                List<HausaufgabenModel> alertHomeworks;
                alertHomeworks = getHomeworksWithAlert();

                if(!alertHomeworks.isEmpty()){
                    if(alertAllowed(HW_ALL_VAR_1, getApplicationContext()) || true){
                        String inhalt = "Zu Erledigen : \n";
                        for (HausaufgabenModel current : alertHomeworks){
                            inhalt = inhalt + "Hausaufgabe : " + current.getHausaufagenName() + "\nFach : " + current.getFach() + "\n\n";
                        }
                        sendNotification(getApplicationContext(), 0, "Hausaufgaben", inhalt, 3);
                    }
                }
            }
        });
        t.start();

        return super.onStartCommand(intent, flags, startId);
    }

    private List<HausaufgabenModel> getHomeworksWithAlert() {
        List<HausaufgabenModel> daten = new ArrayList<>();
        List<HausaufgabenModel> allHomeworks = new ArrayList<>();

        allHomeworks.addAll(HomeworkDB.getInstance(getApplicationContext()).getAllHomework());
        for (HausaufgabenModel current : allHomeworks){
            if(current.getAlarm() == 1){
                daten.add(current);
            }
        }
        return daten;
    }

    public static  HA_AlertService getInstance(){
        return ha_alertService;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
