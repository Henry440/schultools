package de.oliver.schultools.models;

public class AppInfoModel {
    private String name;
    private String inhalt;
    private int actionKey;

    public AppInfoModel(String name, String inhalt, int actionKey) {
        this.name = name;
        this.inhalt = inhalt;
        this.actionKey = actionKey;
    }

    public AppInfoModel(String name, String inhalt) {
        this(name, inhalt, 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInhalt() {
        return inhalt;
    }

    public void setInhalt(String inhalt) {
        this.inhalt = inhalt;
    }

    public int getActionKey() {
        return actionKey;
    }

    public void setActionKey(int actionKey) {
        this.actionKey = actionKey;
    }
}
