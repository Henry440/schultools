package de.oliver.schultools.models;

public class HausaufgabenModel {
    private long ID;

    private String  HausaufagenName;    //PC Komponenten
    private String  Fach;               //Technik Informatik
    private int     Stunde;             //6
    private int     Tag;                //1
    private String  Wo;                 //Buch
    private String  Aufgabe;            // S.36 Nr.5
    private int     Bewertung;          //Auf Note
    private int     Alarm;              //Erinnerung am Abend
    private int     Erledigt;           //Abgeschlossen noch offen?
    private String  Bemerkung;          //Gruppenarbeit

    public HausaufgabenModel(String hausaufagenName, String fach, int stunde, int tag, String wo, String aufgabe, int bewertung, int alarm, int erledigt, String bemerkung) {
        HausaufagenName = hausaufagenName;
        Fach = fach;
        Stunde = stunde;
        Tag = tag;
        Wo = wo;
        Aufgabe = aufgabe;
        Bewertung = bewertung;
        Alarm = alarm;
        Erledigt = erledigt;
        Bemerkung = bemerkung;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getHausaufagenName() {
        return HausaufagenName;
    }

    public void setHausaufagenName(String hausaufagenName) {
        HausaufagenName = hausaufagenName;
    }

    public String getFach() {
        return Fach;
    }

    public void setFach(String fach) {
        Fach = fach;
    }

    public int getStunde() {
        return Stunde;
    }

    public void setStunde(int stunde) {
        Stunde = stunde;
    }

    public int getTag() {
        return Tag;
    }

    public void setTag(int tag) {
        Tag = tag;
    }

    public String getWo() {
        return Wo;
    }

    public void setWo(String wo) {
        Wo = wo;
    }

    public String getAufgabe() {
        return Aufgabe;
    }

    public void setAufgabe(String aufgabe) {
        Aufgabe = aufgabe;
    }

    public int getBewertung() {
        return Bewertung;
    }

    public void setBewertung(int bewertung) {
        Bewertung = bewertung;
    }

    public int getAlarm() {
        return Alarm;
    }

    public void setAlarm(int alarm) {
        Alarm = alarm;
    }

    public int getErledigt() {
        return Erledigt;
    }

    public void setErledigt(int erledigt) {
        Erledigt = erledigt;
    }

    public String getBemerkung() {
        return Bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        Bemerkung = bemerkung;
    }
}
