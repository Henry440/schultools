package de.oliver.schultools.models;

public class AlertManagerModel {
    long id;
    String callername;  //Name Notify_VP_Today
    int lastcallday;    //0 Montag, 1 Dienstag, 2 Mittwoch, 3 Donnerstag, 4 Freitag
    int calls_at_day;   // Max Notification at day
    int posible_calls;  // Rest der Maximalen Notifications

    public AlertManagerModel(long id, String callername, int lastcallday, int calls_at_day, int posible_calls) {
        this.id = id;
        this.callername = callername;
        this.lastcallday = lastcallday;
        this.calls_at_day = calls_at_day;
        this.posible_calls = posible_calls;
    }

    public AlertManagerModel(String callername, int lastcallday, int calls_at_day, int posible_calls) {
        this.callername = callername;
        this.lastcallday = lastcallday;
        this.calls_at_day = calls_at_day;
        this.posible_calls = posible_calls;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCallername() {
        return callername;
    }

    public void setCallername(String callername) {
        this.callername = callername;
    }

    public int getLastcallday() {
        return lastcallday;
    }

    public void setLastcallday(int lastcallday) {
        this.lastcallday = lastcallday;
    }

    public int getCalls_at_day() {
        return calls_at_day;
    }

    public void setCalls_at_day(int calls_at_day) {
        this.calls_at_day = calls_at_day;
    }

    public int getPosible_calls() {
        return posible_calls;
    }

    public void setPosible_calls(int posible_calls) {
        this.posible_calls = posible_calls;
    }
}
