package de.oliver.schultools.models;

public class StundenplanModel {
    private long    id;         //SQL
    private int     Stunde;     //1,2,3,4,5,6,7,8
    private String  Zeit_Von;   //8:00
    private String  Zeit_Bis;   //8:45
    private String  Fach;       //Deutsch
    private String  Raum;       //17
    private int     tag;        //0 (Montag) --> 4 (Freitag)

    public StundenplanModel(int stunde, String zeit_Von, String zeit_Bis, String fach, String raum, int tag) {
        this.Stunde = stunde;
        this.Zeit_Von = zeit_Von;
        this.Zeit_Bis = zeit_Bis;
        this.Fach = fach;
        this.Raum = raum;
        this.tag = tag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getStunde() {
        return Stunde;
    }

    public void setStunde(int stunde) {
        Stunde = stunde;
    }

    public String getZeit_Von() {
        return Zeit_Von;
    }

    public void setZeit_Von(String zeit_Von) {
        Zeit_Von = zeit_Von;
    }

    public String getZeit_Bis() {
        return Zeit_Bis;
    }

    public void setZeit_Bis(String zeit_Bis) {
        Zeit_Bis = zeit_Bis;
    }

    public String getFach() {
        return Fach;
    }

    public void setFach(String fach) {
        Fach = fach;
    }

    public String getRaum() {
        return Raum;
    }

    public void setRaum(String raum) {
        Raum = raum;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }
}
