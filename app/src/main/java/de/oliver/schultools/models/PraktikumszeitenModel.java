package de.oliver.schultools.models;

public class PraktikumszeitenModel {
    private long ID;

    private String name;
    private String zeit_von;
    private String zeit_bis;
    private int position;


    public PraktikumszeitenModel(String name, String zeit_von, String zeit_bis, int position) {
        this.name = name;
        this.zeit_von = zeit_von;
        this.zeit_bis = zeit_bis;
        this.position = position;
    }

    public PraktikumszeitenModel() { }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZeit_von() {
        return zeit_von;
    }

    public void setZeit_von(String zeit_von) {
        this.zeit_von = zeit_von;
    }

    public String getZeit_bis() {
        return zeit_bis;
    }

    public void setZeit_bis(String zeit_bis) {
        this.zeit_bis = zeit_bis;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
