package de.oliver.schultools.models;

public class TimeTableFixModel {
    int stunde;
    String zeit_von;
    String zeit_bis;
    int count;

    public TimeTableFixModel(int stunde, String zeit_von, String zeit_bis) {
        this.stunde = stunde;
        this.zeit_von = zeit_von;
        this.zeit_bis = zeit_bis;
        this.count = 0;
    }

    public TimeTableFixModel() {
        this.stunde = 0;
        this.zeit_von = null;
        this.zeit_bis = null;
        this.count = 0;
    }

    public int getStunde() {
        return stunde;
    }

    public void setStunde(int stunde) {
        this.stunde = stunde;
    }

    public String getZeit_von() {
        return zeit_von;
    }

    public void setZeit_von(String zeit_von) {
        this.zeit_von = zeit_von;
    }

    public String getZeit_bis() {
        return zeit_bis;
    }

    public void setZeit_bis(String zeit_bis) {
        this.zeit_bis = zeit_bis;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
