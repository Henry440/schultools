package de.oliver.schultools.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.models.AppInfoModel;

public class AppInfoAdapter extends ArrayAdapter<AppInfoModel> {

    public AppInfoAdapter(Context context, List<AppInfoModel> objects) {
        super(context, 0, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AppInfoModel current = getItem(position);

        View view = convertView;
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.appinfo_overview, parent, false);
        }

        TextView name = (TextView)view.findViewById(R.id.appinfo_overview_name);
        TextView content = (TextView)view.findViewById(R.id.appinfo_overview_content);

        name.setText(current.getName() + " :");
        content.setText(current.getInhalt());

        return view;
    }
}
