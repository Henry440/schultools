package de.oliver.schultools.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.models.AppInfoModel;
import de.oliver.schultools.models.StundenplanModel;

public class StundenplanAdapter extends ArrayAdapter<StundenplanModel> {

    public StundenplanAdapter(Context context, List<StundenplanModel> objects) {
        super(context, 0, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StundenplanModel current = getItem(position);

        View view = convertView;
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.stundenplan_eintrag_overview, parent, false);
        }

        TextView stunde = (TextView)view.findViewById(R.id.stundenplan_eintrag_stunde);
        TextView fach = (TextView)view.findViewById(R.id.stundenplan_eintrag_fach);
        TextView raum = (TextView)view.findViewById(R.id.stundenplan_eintrag_raum);
        TextView zeit_von = (TextView)view.findViewById(R.id.stundenplan_eintrag_zeit_von);
        TextView zeit_bis = (TextView)view.findViewById(R.id.stundenplan_eintrag_zeit_bis);

        stunde.setText(String.valueOf(current.getStunde()));
        fach.setText(current.getFach());
        raum.setText(current.getRaum());
        zeit_von.setText(current.getZeit_Von() + " Uhr");
        zeit_bis.setText(current.getZeit_Bis() + " Uhr");

        return view;
    }
}
