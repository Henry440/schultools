package de.oliver.schultools.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.models.PraktikumszeitenModel;

public class PraktikumsZeitenAdapter extends ArrayAdapter<PraktikumszeitenModel> {

    public PraktikumsZeitenAdapter(Context context, List<PraktikumszeitenModel> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PraktikumszeitenModel current = getItem(position);

        View view = convertView;
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.praktikum_zeiten_overview, parent, false);
        }

        TextView arbeit = (TextView)view.findViewById(R.id.pr_times_name);
        TextView from = (TextView)view.findViewById(R.id.pr_times_from);
        TextView to = (TextView)view.findViewById(R.id.pr_times_to);

        arbeit.setText(String.valueOf(current.getName()));
        from.setText(String.valueOf(current.getZeit_von()));
        to.setText(String.valueOf(current.getZeit_bis()));

        return view;
    }
}
