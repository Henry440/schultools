package de.oliver.schultools.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.models.HausaufgabenModel;

public class HausaufgabenAdapter extends ArrayAdapter<HausaufgabenModel> {

    public HausaufgabenAdapter(Context context, List<HausaufgabenModel> objects) {
        super(context,0 , objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HausaufgabenModel current = getItem(position);

        String[] day_as_words = { "Mo", "Di", "Mi", "Do", "Fr"};

        View view = convertView;
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.hausaufgaben_eintrag_overview, parent, false);
        }
        TextView ha_name = (TextView)view.findViewById(R.id.hausaufgaben_overview_name);
        TextView ha_fach = (TextView)view.findViewById(R.id.hausaufgaben_overview_fach);
        TextView ha_stunde = (TextView)view.findViewById(R.id.hausaufgaben_overview_stunde);
        TextView ha_tag = (TextView)view.findViewById(R.id.hausaufgaben_overview_tag);
        TextView ha_wo = (TextView)view.findViewById(R.id.hausaufgaben_overview_wo);
        TextView ha_aufgabe = (TextView)view.findViewById(R.id.hausaufgaben_overview_aufgabe);
        TextView ha_bemerkung = (TextView)view.findViewById(R.id.hausaufgaben_overview_bemerkung);

        CheckBox ha_bewertung = (CheckBox)view.findViewById(R.id.hausaufgaben_overview_bewertung);
        CheckBox ha_alarm = (CheckBox)view.findViewById(R.id.hausaufgaben_overview_alarm);
        CheckBox ha_erledigt = (CheckBox)view.findViewById(R.id.hausaufgaben_overview_erledigt);

        ha_name.setText(String.valueOf(current.getHausaufagenName()));
        ha_fach.setText(String.valueOf(current.getFach()));
        ha_stunde.setText(String.valueOf(current.getStunde()));
        ha_tag.setText(String.valueOf(day_as_words[current.getTag()]));
        ha_wo.setText(String.valueOf(current.getWo()));
        ha_aufgabe.setText(String.valueOf(current.getAufgabe()));
        ha_bemerkung.setText(String.valueOf(current.getBemerkung()));

        if(current.getBewertung() == 1){
            ha_bewertung.setChecked(true);
        }else{
            ha_bewertung.setChecked(false);
        }

        if(current.getAlarm() == 1){
            ha_alarm.setChecked(true);
        }else{
            ha_alarm.setChecked(false);
        }

        if(current.getErledigt() == 1){
            ha_erledigt.setChecked(true);
        }else{
            ha_erledigt.setChecked(false);
        }

        return view;
    }


}
