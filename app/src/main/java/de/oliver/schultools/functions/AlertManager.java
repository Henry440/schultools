package de.oliver.schultools.functions;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.database.AlertManagerDB;
import de.oliver.schultools.models.AlertManagerModel;

import static de.oliver.schultools.functions.DateAndTimes.get_day_as_numer;

public class AlertManager {
    public static boolean alertAllowed(String caller, Context c){
        boolean ret = false;
        List<AlertManagerModel> datas = new ArrayList<>();
        datas.addAll(AlertManagerDB.getInstance(c).getAllAlertModels());
        for(AlertManagerModel current : datas){
            if(current.getCallername() == null){
                break;
            }
            if(current.getCallername().equals(caller)){

                if(current.getLastcallday() == get_day_as_numer()){
                    if(current.getPosible_calls() > 0){
                        current.setPosible_calls(current.getPosible_calls() - 1);
                        AlertManagerDB.getInstance(c).updateAlertModel(current);
                        ret = true;
                        break;
                    }else{
                        break;
                    }
                }else{
                    current.setPosible_calls(current.getCalls_at_day() - 1);
                    ret = true;
                    current.setLastcallday(get_day_as_numer());
                    AlertManagerDB.getInstance(c).updateAlertModel(current);
                    break;
                }

            }
        }
        return ret;
    }

    public static int getRestCounts(String caller, Context c){
        List<AlertManagerModel> datas = new ArrayList<>();
        datas.addAll(AlertManagerDB.getInstance(c).getAllAlertModels());
        for(AlertManagerModel current : datas){
            if(current.getCallername() == null){
                break;
            }
            if(current.getCallername().equals(caller)){
                return  current.getPosible_calls();
            }
        }
        return 0;
    }

    public static void correctCounts(String caller, Context c, int chances){
        List<AlertManagerModel> datas = new ArrayList<>();
        datas.addAll(AlertManagerDB.getInstance(c).getAllAlertModels());
        for(AlertManagerModel current : datas){
            if(current.getCallername() == null){
                break;
            }
            if(current.getCallername().equals(caller)){
                current.setPosible_calls(chances);
                AlertManagerDB.getInstance(c).updateAlertModel(current);
            }
        }
    }
}
