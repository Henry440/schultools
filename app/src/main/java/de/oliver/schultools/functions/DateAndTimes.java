package de.oliver.schultools.functions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateAndTimes {

    public static String get_day_and_month_asString(){
        String ret = "";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("d-MM");
        Date d = new Date();
        d.setTime(c.getTimeInMillis());
        ret = df.format(d);
        return ret;
    }

    public static String get_day_and_month_asString_next(){
        String ret = "";
        Calendar c = Calendar.getInstance();
        long time = c.getTimeInMillis();
        time = time + (1000 * 60 * 60 * 24);
        c.setTimeInMillis(time);
        SimpleDateFormat df = new SimpleDateFormat("d-MM");
        Date d = new Date();
        d.setTime(c.getTimeInMillis());
        ret = df.format(d);
        return ret;
    }

    public static String get_day_and_month_asString_next_monday(int add_day){
        String ret = "";
        Calendar c = Calendar.getInstance();
        long time = c.getTimeInMillis();
        time = time + (1000 * 60 * 60 * 24 * add_day);
        c.setTimeInMillis(time);
        SimpleDateFormat df = new SimpleDateFormat("d-MM");
        Date d = new Date();
        d.setTime(c.getTimeInMillis());
        ret = df.format(d);
        return ret;
    }

    public static int get_day_as_numer(){
        //LOw Chance
        String ret_raw = "0";
        int ret = 0;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("u");
        Date d = new Date();
        d.setTime(c.getTimeInMillis());
        ret_raw = df.format(d);

        ret = Integer.parseInt(ret_raw);

        return ret;
    }

    public static int get_hour_from_day(){
        String ret_raw = "0";
        int ret = 0;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("H");
        Date d = new Date();
        d.setTime(c.getTimeInMillis());
        ret_raw = df.format(d);

        ret = Integer.parseInt(ret_raw);

        return ret;
    }

    public static Date get_hour_and_minutes_from_day(){
        String pat = "HH:mm";
        Date ret = null;
        String two;
        SimpleDateFormat pattern = new SimpleDateFormat(pat);
        Calendar c = Calendar.getInstance();
        ret = c.getTime();
        two = pattern.format(ret);
        ret = String_to_Date(pat, two);
        return String_to_Date(pat, two);
    }

    public static Date String_to_Date(String pattern, String time){
        Date ret = null;
        DateFormat form = new SimpleDateFormat(pattern);

        try {
            ret = form.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return ret;
    }

    //TODO Make mor Time Functions need in Future time in Hours and Minutes
}
