package de.oliver.schultools.functions;

import android.content.Context;
import android.content.SharedPreferences;

import static de.oliver.schultools.var.SharedPreferencesVars.spAppDataKey;
import static de.oliver.schultools.var.SharedPreferencesVars.spBoolGetNotifyAll;
import static de.oliver.schultools.var.SharedPreferencesVars.spBoolGetNotifyHomeWorkAlert;
import static de.oliver.schultools.var.SharedPreferencesVars.spBoolGetNotifyLessonStart;
import static de.oliver.schultools.var.SharedPreferencesVars.spBoolHasInetCon;
import static de.oliver.schultools.var.SharedPreferencesVars.spBoolIsFree;
import static de.oliver.schultools.var.SharedPreferencesVars.spBoolIsSchool;
import static de.oliver.schultools.var.SharedPreferencesVars.spBoolTDVP;
import static de.oliver.schultools.var.SharedPreferencesVars.spBoolTMVP;
import static de.oliver.schultools.var.SharedPreferencesVars.spBoolUsedInet;
import static de.oliver.schultools.var.SharedPreferencesVars.spBoolgetNotifyVP;
import static de.oliver.schultools.var.user_vars.GeneralVars.allowUsedInternet;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationAll;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationHomeWorkAlert;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationLessonStart;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationVP;
import static de.oliver.schultools.var.user_vars.GeneralVars.hasInternetConnection;
import static de.oliver.schultools.var.user_vars.GeneralVars.isFree;
import static de.oliver.schultools.var.user_vars.GeneralVars.isSchool;
import static de.oliver.schultools.var.user_vars.GeneralVars.vPavivabelTD;
import static de.oliver.schultools.var.user_vars.GeneralVars.vPavivabelTM;


public class ManageSharedPreferences {

    public static void setSharedPreferences(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(spAppDataKey, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(spBoolIsSchool, true);
        editor.putBoolean(spBoolIsFree, false);

        editor.putBoolean(spBoolGetNotifyAll, true);
        editor.putBoolean(spBoolgetNotifyVP, true);
        editor.putBoolean(spBoolGetNotifyLessonStart, true);
        editor.putBoolean(spBoolGetNotifyHomeWorkAlert, true);

        editor.putBoolean(spBoolUsedInet, true);
        editor.putBoolean(spBoolHasInetCon, true);

        editor.putBoolean(spBoolTDVP, true);
        editor.putBoolean(spBoolTMVP, true);

        editor.commit();

        getSharedPreferences(context);
    }

    public static void loadSH(Context context){
        getSharedPreferences(context);
    }

    public static void getSharedPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(spAppDataKey, context.MODE_PRIVATE);

        isSchool = sharedPreferences.getBoolean(spBoolIsSchool, true);
        isFree = sharedPreferences.getBoolean(spBoolIsFree, false);

        getNotificationAll = sharedPreferences.getBoolean(spBoolGetNotifyAll, true);
        getNotificationVP = sharedPreferences.getBoolean(spBoolgetNotifyVP, true);
        getNotificationLessonStart = sharedPreferences.getBoolean(spBoolGetNotifyLessonStart, true);
        getNotificationHomeWorkAlert = sharedPreferences.getBoolean(spBoolGetNotifyHomeWorkAlert, true);

        allowUsedInternet = sharedPreferences.getBoolean(spBoolUsedInet, true);
        hasInternetConnection = sharedPreferences.getBoolean(spBoolHasInetCon, true);

        vPavivabelTD = sharedPreferences.getBoolean(spBoolTDVP, true);
        vPavivabelTM = sharedPreferences.getBoolean(spBoolTMVP, true);

    }

    public static void saveSharedPreferences(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(spAppDataKey, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(spBoolIsSchool, isSchool);
        editor.putBoolean(spBoolIsFree, isFree);

        editor.putBoolean(spBoolGetNotifyAll, getNotificationAll);
        editor.putBoolean(spBoolgetNotifyVP, getNotificationVP);
        editor.putBoolean(spBoolGetNotifyLessonStart, getNotificationLessonStart);
        editor.putBoolean(spBoolGetNotifyHomeWorkAlert, getNotificationHomeWorkAlert);

        editor.putBoolean(spBoolUsedInet, allowUsedInternet);
        editor.putBoolean(spBoolHasInetCon, hasInternetConnection);

        editor.putBoolean(spBoolTDVP, vPavivabelTD);
        editor.putBoolean(spBoolTMVP, vPavivabelTM);

        editor.commit();

    }


}
