package de.oliver.schultools.functions;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import de.oliver.schultools.R;

import static de.oliver.schultools.var.NotificationVars.CHANNEL_HW_ID;
import static de.oliver.schultools.var.NotificationVars.CHANNEL_LS_ID;
import static de.oliver.schultools.var.NotificationVars.CHANNEL_TEST_ID;
import static de.oliver.schultools.var.NotificationVars.CHANNEL_VP_ID;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationHomeWorkAlert;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationLessonStart;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationVP;

public class NotificationHandling {
    private static NotificationManagerCompat notificationManagerCompat;

    public static void createNotificationChannels(Context inContext) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channelVP = new NotificationChannel(CHANNEL_VP_ID, "Vertretungsplan", NotificationManager.IMPORTANCE_DEFAULT);
            channelVP.setDescription("Benachichtigt dich wenn ein neuer Vertretungsplan verfügbar ist");
            channelVP.enableVibration(true);
            channelVP.enableLights(true);

            NotificationChannel channelLS = new NotificationChannel(CHANNEL_LS_ID, "Schulstunden", NotificationManager.IMPORTANCE_LOW);
            channelLS.setDescription("Benachichtigt dich über deinen Stundenplan");
            channelLS.enableVibration(true);

            NotificationChannel channelHW = new NotificationChannel(CHANNEL_HW_ID, "Hausaufgaben", NotificationManager.IMPORTANCE_DEFAULT);
            channelHW.setDescription("Benachichtigt dich über deine Hausaufgaben sofern es Eingestellt ist");
            channelHW.enableVibration(true);
            channelHW.enableLights(true);

            NotificationChannel channelTEST = new NotificationChannel(CHANNEL_TEST_ID, "Test", NotificationManager.IMPORTANCE_DEFAULT);
            channelTEST.setDescription("Über den Chanel kommen Test Nachichten für die AppEntwicklung");
            channelTEST.enableVibration(true);
            channelTEST.enableLights(true);

            NotificationManager manager = inContext.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channelTEST);
            manager.createNotificationChannel(channelVP);
            manager.createNotificationChannel(channelLS);
            manager.createNotificationChannel(channelHW);


        }
    }

    public static void sendNotification(Context c,int channel, String title ,String msg, int msg_id){
        notificationManagerCompat = NotificationManagerCompat.from(c);

        if(channel == 0){ //TestChannel
            Notification notification = new NotificationCompat.Builder(c, CHANNEL_TEST_ID)
                    .setSmallIcon(R.drawable.ic_notification_test)
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .build();
            notificationManagerCompat.notify(msg_id, notification);
        }else if(channel == 1){//VP Channel
            Notification notification = new NotificationCompat.Builder(c, CHANNEL_VP_ID)
                    .setSmallIcon(R.drawable.ic_notify_vp)
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .build();
            if(getNotificationVP){
                notificationManagerCompat.notify(msg_id, notification);
            }
            System.out.println("YES : " + getNotificationVP);
        }else if(channel == 2){// Leasson Channel
            Notification notification = new NotificationCompat.Builder(c, CHANNEL_LS_ID)
                    .setSmallIcon(R.drawable.ic_directions_walk_black_24dp)
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .build();
            if(getNotificationLessonStart){
                notificationManagerCompat.notify(msg_id, notification);
            }
        }else if(channel == 3){// Homework Channel
            Notification notification = new NotificationCompat.Builder(c, CHANNEL_HW_ID)
                    .setSmallIcon(R.drawable.ic_notify_ha)
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .build();
            if(getNotificationHomeWorkAlert){
                notificationManagerCompat.notify(msg_id, notification);
            }
        }
    }

}
