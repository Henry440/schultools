package de.oliver.schultools.functions;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.activity.AppInfo;
import de.oliver.schultools.activity.MainActivity;
import de.oliver.schultools.database.AlertManagerDB;
import de.oliver.schultools.models.AlertManagerModel;

import static de.oliver.schultools.functions.DateAndTimes.get_day_as_numer;
import static de.oliver.schultools.functions.ManageSharedPreferences.getSharedPreferences;
import static de.oliver.schultools.functions.ManageSharedPreferences.setSharedPreferences;
import static de.oliver.schultools.functions.NotificationHandling.createNotificationChannels;
import static de.oliver.schultools.var.AlertManagerVars.*;
import static de.oliver.schultools.var.SharedPreferencesVars.spFirstStart;

public class FirstAppStart {
    private static Context inContext;

    public static void manageFirstAppStart(Context context){
        inContext = context;
        SharedPreferences sharedPreferences = inContext.getSharedPreferences(spFirstStart, Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        if(sharedPreferences.getBoolean(spFirstStart, false) == false){
            sharedPreferencesEditor.putBoolean(spFirstStart, true);
            sharedPreferencesEditor.commit();
            doFirstStartAction(inContext);
        }else{
            getSharedPreferences(inContext);
        }

    }

    public static void doFirstStartAction(Context context) {
        setSharedPreferences(context);
        createNotificationChannels(context);
        getAppPermissions(context);
        inizialAlertManager();
    }

    private static void inizialAlertManager() {
        AlertManagerDB.getInstance(inContext).deletaAllAlertModels();
        List<AlertManagerModel> models = new ArrayList<>();
        models.add(new AlertManagerModel(VP_TODAY_VAR_1, get_day_as_numer(), 1,1));
        models.add(new AlertManagerModel(VP_TODAY_VAR_2, get_day_as_numer(), 1,1));
        models.add(new AlertManagerModel(VP_TODAY_VAR_3, get_day_as_numer(), 1,1));

        models.add(new AlertManagerModel(VP_TOMORROW_VAR_1, get_day_as_numer(), 1,1));
        models.add(new AlertManagerModel(VP_TOMORROW_VAR_2, get_day_as_numer(), 1,1));
        models.add(new AlertManagerModel(VP_TOMORROW_VAR_3, get_day_as_numer(), 1,1));

        models.add(new AlertManagerModel(VP_MONDAY_VAR_1, get_day_as_numer(), 1,1));
        models.add(new AlertManagerModel(VP_MONDAY_VAR_2, get_day_as_numer(), 1,1));
        models.add(new AlertManagerModel(VP_MONDAY_VAR_3, get_day_as_numer(), 1,1));

        models.add(new AlertManagerModel(HW_ALL_VAR_1, get_day_as_numer(), 1,1));
        models.add(new AlertManagerModel(HW_ALL_VAR_2, get_day_as_numer(), 1,1));
        models.add(new AlertManagerModel(HW_ALL_VAR_3, get_day_as_numer(), 1,1));

        models.add(new AlertManagerModel(TEST_NV_VAR_1, get_day_as_numer(), 0,0));

        for(AlertManagerModel current : models){
            AlertManagerDB.getInstance(inContext).creatAlertModel(current);
        }
    }

    public static final int REQUESTCODE_EXT_STO = 44;

    public static void getAppPermissions(Context contex){
        AppInfo act = new AppInfo();

        if(ActivityCompat.checkSelfPermission(contex, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            System.out.println("PERMISSION ERLAUBT");
        }else {
            ActivityCompat.requestPermissions(MainActivity.getInstance(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUESTCODE_EXT_STO);
        }
    }



}
