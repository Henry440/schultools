package de.oliver.schultools.functions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import de.oliver.schultools.activity.MainActivity;
import de.oliver.schultools.services.VP_CheckService;

import static de.oliver.schultools.functions.CheckNetworks.mobileHaveINet;
import static de.oliver.schultools.functions.DateAndTimes.get_day_and_month_asString;
import static de.oliver.schultools.functions.DateAndTimes.get_day_and_month_asString_next;
import static de.oliver.schultools.functions.DateAndTimes.get_day_and_month_asString_next_monday;
import static de.oliver.schultools.functions.DateAndTimes.get_day_as_numer;
import static de.oliver.schultools.functions.ManageSharedPreferences.loadSH;
import static de.oliver.schultools.functions.ManageSharedPreferences.saveSharedPreferences;
import static de.oliver.schultools.var.user_vars.GeneralVars.allowUsedInternet;
import static de.oliver.schultools.var.user_vars.GeneralVars.hasInternetConnection;
import static de.oliver.schultools.var.user_vars.GeneralVars.vPavivabelTD;
import static de.oliver.schultools.var.user_vars.GeneralVars.vPavivabelTM;

public class DownloadVP {
    /*
    *------------------Variablen------------------------------
     */
    //Vars manage
    private static int aktuellerTag;
    private static int          searchDay;
    private static boolean      downloadSucess  = false;
    private static Context      VPcontext       = VP_CheckService.getInstance().getApplicationContext();
    public static boolean[]     verfuegbar_VP   = {false, false, false};
    private static boolean      check_bool      = false;
    private static boolean      chance          = false;
    public static boolean[]     chance_in_arry  = {false, false, false};
    public static boolean[][]   chanche_in_vp   = {{false, false}, {false,false}, {false,false}};
    public static boolean[][]   clear_chance    = {{false, false}, {false,false}, {false,false}};
    private static boolean      retryDownload           = true;

    private static File pdffile;

    //Vars URL
    private static String BasicURL              = "https://www.ags-erfurt.de/images/Vertretungsplan/Huegelschule/H ";
    private static String[] URL_Day_String      = {"Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"};

    /*
     *------------------Before Download---------------------
     */
    public static boolean startDownload(Context c, int requestDay){  //requestDay 0=Today / 1=Tomorrow / 2=nextMonday
        VPcontext = c;
        searchDay = requestDay;
        checkVerfuegbar(VPcontext);
        MainActivity.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(verfuegbar_VP[searchDay]){
                    getTheFile();
                }else{
                    Toast.makeText(VPcontext, "Download nicht möglich", Toast.LENGTH_SHORT).show();
                    downloadSucess = false;
                }
            }
        });
        System.out.println("YES TASK");
        afterDownload();
        return getDownloadstate();
    }

    public static void initVerfuegbarVars(){
        if(get_day_as_numer() < 5){
            verfuegbar_VP[0] = vPavivabelTD;
            verfuegbar_VP[1] = vPavivabelTM;
            verfuegbar_VP[2] = false;
        }else if (get_day_as_numer() == 5){
            verfuegbar_VP[0] = vPavivabelTD;
            verfuegbar_VP[1] = false;
            verfuegbar_VP[2] = vPavivabelTM;
        }else if(get_day_as_numer() == 6){
            verfuegbar_VP[0] = false;
            verfuegbar_VP[1] = false;
            verfuegbar_VP[2] = vPavivabelTM;
        }else {
            verfuegbar_VP[0] = false;
            verfuegbar_VP[1] = false;
            verfuegbar_VP[2] = vPavivabelTM;
        }
    }

    public static void checkVerfuegbar(Context context) {
        String vpheute = "";
        String vpmorgen = "";
        String vpmontag = "";

        mobileHaveINet(context);
        VPcontext = context;
        loadSH(context);
        initVerfuegbarVars();
        if(!hasInternetConnection && !allowUsedInternet){
            System.out.println("RETURN !!! XD");
            return;
        }
        boolean[] to_check = {true, true, true};
        chanche_in_vp = clear_chance;
        //Generroere Benötigte URLs
        if(get_day_as_numer() <= 4){
            vpheute = makeURLString(0);
            vpmorgen = makeURLString(1);
        }else if(get_day_as_numer() == 5){
            vpheute = makeURLString(0);
            vpmontag = makeURLString(2);
        }else{
            vpmontag = makeURLString(2);
        }



        //Prüfen welche URLs gebraucht werden
        if(vpheute.equals("")){
            to_check[0] = false;
            verfuegbar_VP[0] = false;
        }
        if(vpmorgen.equals("")){
            to_check[1] = false;
            verfuegbar_VP[1] = false;
        }
        if(vpmontag.equals("")){
            to_check[2] = false;
            verfuegbar_VP[2] = false;
        }

        System.out.println("BOOLS " + to_check[0] + " - " + to_check[1] + " - " + to_check[2]);

        if(to_check[0]){
            if(checkVPstatus(vpheute)){
                if(!verfuegbar_VP[0]){
                    chanche_in_vp[0][0] = true;
                }
                verfuegbar_VP[0] = true;
            }else{
                if(verfuegbar_VP[0]){
                    chanche_in_vp[0][1] = true;
                }
                verfuegbar_VP[0] = false;
            }
        }

        if(to_check[1]){
            if(checkVPstatus(vpmorgen)){
                if(!verfuegbar_VP[1]){
                    chanche_in_vp[1][0] = true;
                }
                verfuegbar_VP[1] = true;
            }else{
                if(verfuegbar_VP[1]){
                    chanche_in_vp[1][1] = true;
                }
                verfuegbar_VP[1] = false;
            }
        }

        if(to_check[2]){
            if(checkVPstatus(vpmontag)){
                if(!verfuegbar_VP[2]){
                    chanche_in_vp[2][0] = true;
                }
                verfuegbar_VP[2] = true;
            }else{
                if(verfuegbar_VP[2]){
                    chanche_in_vp[2][1] = true;
                }
                verfuegbar_VP[2] = false;
            }
        }
        saveSharedPreferences(context);
        System.out.println(chanche_in_vp[0][0]);

    }

    private static String makeURLString(int day) {
        String ret = "";
        String block = "";
        ret = BasicURL;

        int current_day = get_day_as_numer();
        int search = 0;

        if(day == 0){//Heute
            block = get_day_and_month_asString();
            search = current_day - 1;
        }else if(day == 1){//Morgen
            block = get_day_and_month_asString_next();
            if(current_day == 7){
                search = 0;
            }else{
                search = current_day;
            }
        }else{//Montag
            if(current_day == 5){//Freitag
                block = get_day_and_month_asString_next_monday(3);
            }else if(current_day == 6){//Samstag
                block = get_day_and_month_asString_next_monday(2);
            }else{//Sontag
                return makeURLString(1);//Recursion call same function for Next day
            }
            search = 0;
        }
        block = correcktBlock(block);
        ret = ret + block + " ";
        ret = ret + URL_Day_String[search];
        ret = ret + ".pdf";
        return ret;

    }

    private static String correcktBlock(String block){
        String[] block_split = block.split("-");
        int tag = Integer.parseInt(block_split[0]);
        int month = Integer.parseInt(block_split[1]);

        if(tag <= 9){
            if(month <= 9){
                block = "0" + String.valueOf(tag) + "-0" + String.valueOf(month);
            }else{
                block = "0" + String.valueOf(tag) + "-" + String.valueOf(month);
            }
        }else{
            if(month <= 9){
                block = String.valueOf(tag) + "-0" + String.valueOf(month);
            }else{
                block = String.valueOf(tag) + "-" + String.valueOf(month);
            }
        }

        return block;
    }

    private static boolean checkVPstatus(final String URLtoFile){
        Thread t;
        t = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Starte Download : " + URLtoFile);
                        //Variablen Setzen
                        String filename = "vp" + System.currentTimeMillis() + ".pdf";
                        String folderforfile = "Vertretungsplan";

                        int sizeInputStream = 8192;

                        try {

                            //String to URL for File
                            URL fileURL = new URL(URLtoFile);
                            URLConnection connection = fileURL.openConnection();
                            connection.connect();

                            InputStream input = new BufferedInputStream(connection.getInputStream(), sizeInputStream);
                            check_bool = true;

                        }catch (Exception e){
                            e.printStackTrace();
                            check_bool = false;
                        }
                    }
                }
        );

        t.start();
        try {
            t.join();
            System.out.println("YES : JOIN");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return check_bool;
    }


    /*
     *-----------------Download------------------------------
     */
    private static void getTheFile(){
        Thread t = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        downloadSucess = true;

                        int sizeInputStream = 8192;
                        int bytebuffer = 1024;
                        String filename = "vp" + System.currentTimeMillis() + ".pdf";
                        String folderforfile = "Vertretungsplan_New";

                        //DownloadBereich
                        try {

                            //String to URL for File
                            URL fileURL = new URL(makeURLString(searchDay));
                            System.out.println("YES : URL : " + makeURLString(searchDay));
                            URLConnection connection = fileURL.openConnection();
                            connection.connect();
                            System.out.println("YES CONNECT");
                            InputStream input = new BufferedInputStream(connection.getInputStream(), sizeInputStream);

                            File downloadfolder = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), folderforfile);
                            if(!downloadfolder.exists()){
                                downloadfolder.mkdirs();
                            }
                            pdffile = new File(downloadfolder, filename);

                            OutputStream output = new FileOutputStream(pdffile);

                            byte[] buffer = new byte[bytebuffer];
                            int read;

                            while((read = input.read(buffer)) != -1){
                                output.write(buffer, 0, read);
                            }

                            output.flush();
                            output.close();
                            input.close();
                            downloadSucess = true;
                            System.out.println("YES SUCESS " + downloadSucess);
                        }catch (Exception e){
                            e.printStackTrace();
                            downloadSucess = false;
                        }finally {
                            chance = true;
                        }
                    }
                }
                );

        t.start();

        try {
            t.join();
            System.out.println("YES - JOIN");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    /*
     *------------------After Download---------------------
     */
    private static void afterDownload(){
        System.out.println("YES - After Download");
        if(!downloadSucess){
            retryDownload = false;
            startDownload(MainActivity.getInstance().getApplicationContext(), searchDay);
        }else {
            retryDownload = true;
            System.out.println("YES AFTER DOWNLOAD " + downloadSucess);
            if (downloadSucess) {
                Intent target = new Intent(Intent.ACTION_VIEW);
                try {
                    target.setDataAndType(Uri.fromFile(pdffile), "application/pdf");
                    target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                }catch (Exception e){
                    (MainActivity.getInstance()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.getInstance().getApplicationContext(), "Berechtigung Speicher fehlt", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                Intent intent = Intent.createChooser(target, "Open File");

                try {
                    if(target.getType().equals("application/pdf")) {
                        MainActivity.getInstance().getApplicationContext().startActivity(intent);
                    }else{
                        (MainActivity.getInstance()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.getInstance().getApplicationContext(), "Download Fehlgeschlagen", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (Exception e) {
                    System.out.println("YES CATCH");
                    e.printStackTrace();
                }
            } else {
                (MainActivity.getInstance()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.getInstance().getApplicationContext(), "Download Fehlgeschlagen", Toast.LENGTH_SHORT).show();
                    }
                });
            }
       }
    }


    /*
     *------------------Help Functions---------------------
     */
    private static boolean getDownloadstate(){
        return downloadSucess;
    }

}
