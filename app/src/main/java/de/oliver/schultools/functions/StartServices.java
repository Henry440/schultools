package de.oliver.schultools.functions;

import android.content.Context;
import android.content.Intent;

import de.oliver.schultools.activity.MainActivity;
import de.oliver.schultools.services.HA_AlertService;
import de.oliver.schultools.services.VP_CheckService;

import static de.oliver.schultools.var.user_vars.ServiceVars.ServicesHA;
import static de.oliver.schultools.var.user_vars.ServiceVars.ServicesRunning;
import static de.oliver.schultools.var.user_vars.ServiceVars.ServicesVP;

public class StartServices {
    public static void startServices(Context context){
        if(ServicesRunning){
            if(ServicesVP) {
                Intent startVP_Service = new Intent(MainActivity.getInstance().getApplicationContext(), VP_CheckService.class);
                MainActivity.getInstance().getApplicationContext().startService(startVP_Service);
            }
            if(ServicesHA){
                Intent startHA_Service = new Intent(MainActivity.getInstance().getApplicationContext(), HA_AlertService.class);
                MainActivity.getInstance().getApplicationContext().startService(startHA_Service);
            }
        }
    }
}
