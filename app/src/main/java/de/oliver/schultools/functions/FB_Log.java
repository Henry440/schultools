package de.oliver.schultools.functions;

import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

import static de.oliver.schultools.activity.MainActivity.mFirebaseAnalytics;

public class FB_Log {
    public static void LogToFireBase(String msg){
        mFirebaseAnalytics.logEvent(msg,null);
    }

}
