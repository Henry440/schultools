package de.oliver.schultools.functions;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import static android.support.v4.content.ContextCompat.getSystemService;
import static de.oliver.schultools.var.user_vars.GeneralVars.hasInternetConnection;

public class CheckNetworks {
    public static void mobileHaveINet(Context c){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        hasInternetConnection = activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
