package de.oliver.schultools.activity;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.database.TimetableDB;
import de.oliver.schultools.fragments.schule.Schule_Stundenplan;
import de.oliver.schultools.models.StundenplanModel;

public class StundenplanBearbeitenActivity extends AppCompatActivity {

    Toolbar toolbar;
    Spinner spinner_tage, spinner_stunde;
    EditText et_fach, et_raum, et_von, et_bis;
    Button btn_save, btn_load_datas, btn_del;

    TimePickerDialog timePickerDialog;
    Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stundenplan_bearbeiten);
        initComponents();

        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            int day = Integer.parseInt(intent.getStringExtra("day"));
            int lesson = Integer.parseInt(intent.getStringExtra("lesson"));
            int newEntry = Integer.parseInt(intent.getStringExtra("newEntry"));

            setSpinner(day, lesson, newEntry);
        }


    }

    private void setSpinner(int day, int lesson, int newEntry) {
        spinner_tage.setSelection(day);
        spinner_stunde.setSelection(lesson);
        if(newEntry != 1){
            loadDatas();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Schule_Stundenplan.updateLVDatas(this);
    }

    private void initComponents() {
        toolbar = (Toolbar)findViewById(R.id.StundenplanBearbeitenToolbar);
        toolbar.setSubtitle("Stundenplan Bearbeiten");
        spinner_tage = (Spinner)findViewById(R.id.stundenplan_bearbeiten_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.stundenplanspinnerdays, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_tage.setAdapter(adapter);

        spinner_stunde = (Spinner)findViewById(R.id.stundenplan_bearbeiten_spinner_stunde);
        ArrayAdapter<CharSequence> adapter_stunde = ArrayAdapter.createFromResource(this, R.array.stundenplanspinnerstunde, android.R.layout.simple_spinner_item);
        adapter_stunde.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_stunde.setAdapter(adapter_stunde);

        et_fach = (EditText)findViewById(R.id.stundenplan_bearbeiten_et_fach);
        et_raum = (EditText)findViewById(R.id.stundenplan_bearbeiten_et_raum);
        et_von = (EditText)findViewById(R.id.stundenplan_bearbeiten_et_von);
        et_bis = (EditText)findViewById(R.id.stundenplan_bearbeiten_et_bis);

        btn_del = (Button)findViewById(R.id.stundenplan_bearbeiten_btn_del);
        btn_save = (Button)findViewById(R.id.stundenplan_bearbeiten_btn_save);
        btn_load_datas = (Button)findViewById(R.id.stundenplan_bearbeiten_btn_loaddatas);


    }

    private void loadDatas(){
        List<StundenplanModel> datas = TimetableDB.getInstance(this).getAllStunden();
        int Tag = spinner_tage.getSelectedItemPosition();
        int Stunde = spinner_stunde.getSelectedItemPosition();

        boolean sucess = false;

        StundenplanModel treffer = null;

        for(StundenplanModel current : datas){
            if(current.getTag() == Tag){
                if(current.getStunde() == Stunde){
                    sucess = true;
                    treffer = current;
                }
            }
            if (sucess){
                break;
            }
        }

        if(sucess){
            et_fach.setText(treffer.getFach());
            et_raum.setText(treffer.getRaum());
            et_von.setText(treffer.getZeit_Von());
            et_bis.setText(treffer.getZeit_Bis());
            Toast.makeText(this, "Erfolgreich geladen", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Kein Treffer in der Datenbank", Toast.LENGTH_SHORT).show();
        }

    }

    private void delData(){

        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(this);
        alertdialog.setTitle("Wirklich Löschen");
        alertdialog.setMessage("Wollen sie wirklich den Eintag im Stundenplan löschen");
        alertdialog.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
            boolean sucess = false;

            int tag = spinner_tage.getSelectedItemPosition();
            int stunde = spinner_stunde.getSelectedItemPosition();

            @Override
            public void onClick(DialogInterface dialog, int which) {
                List<StundenplanModel> datas = TimetableDB.getInstance(getApplicationContext()).getAllStunden();
                StundenplanModel treffer = null;

                for(StundenplanModel current : datas){
                    if(current.getTag() == tag){
                        if(current.getStunde() == stunde){
                            treffer = current;
                            sucess = true;
                        }
                    }
                    if (sucess){
                        break;
                    }
                }

                if(sucess){
                    TimetableDB.getInstance(getApplicationContext()).deleteStunde(treffer);
                    Toast.makeText(getApplicationContext(), "Erfolgreich Gelöscht", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Stunde Existiert nicht", Toast.LENGTH_SHORT).show();
                }

                Schule_Stundenplan.updateLVDatas(getApplicationContext());
            }
        });

        alertdialog.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "Löschen Abgebrochen", Toast.LENGTH_SHORT).show();
            }
        });

        alertdialog.setCancelable(false);
        alertdialog.create().show();
    }

    private void saveData(){
        boolean can_save_start = false;
        boolean is_an_chance = false;

        List<StundenplanModel> datas = TimetableDB.getInstance(this).getAllStunden();

        int tag = spinner_tage.getSelectedItemPosition();
        int stunde = spinner_stunde.getSelectedItemPosition();

        String to_save_fach = String.valueOf(et_fach.getText());
        String to_save_raum = String.valueOf(et_raum.getText());
        String to_save_von = String.valueOf(et_von.getText());
        String to_save_bis = String.valueOf(et_bis.getText());

        if(to_save_fach != null){
            if(to_save_raum != null){
                if(to_save_von != null){
                    if(to_save_bis != null){
                        can_save_start = true;
                    }else{
                        Toast.makeText(this, "Bis darf nicht leer sein", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this, "Von darf nicht leer sein", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(this, "Raum darf nicht leer sein", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "Fach darf nicht leer sein", Toast.LENGTH_SHORT).show();
        }

        StundenplanModel treffer = null;

        for(StundenplanModel current : datas){
            if(current.getTag() == tag){
                if(current.getStunde() == stunde){
                    is_an_chance = true;
                    treffer = current;
                }
            }
            if (is_an_chance){
                break;
            }
        }

        if(is_an_chance){
            treffer.setTag(tag);
            treffer.setStunde(stunde);
            treffer.setFach(to_save_fach);
            treffer.setRaum(to_save_raum);
            treffer.setZeit_Von(to_save_von);
            treffer.setZeit_Bis(to_save_bis);
            TimetableDB.getInstance(this).updateStunde(treffer);
            Toast.makeText(this, "Stunde wurde erfolgreich geändert", Toast.LENGTH_SHORT).show();
        }else{
            StundenplanModel newStunde = new StundenplanModel(stunde, to_save_von, to_save_bis, to_save_fach, to_save_raum, tag);
            TimetableDB.getInstance(this).creatStunde(newStunde);
            Toast.makeText(this, "Stunde wurde erfolgreich angelegt", Toast.LENGTH_SHORT).show();
        }

        Schule_Stundenplan.updateLVDatas(this);

    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.stundenplan_bearbeiten_btn_del:
                delData();
                break;

            case R.id.stundenplan_bearbeiten_btn_save:
                saveData();
                StundenplanBearbeitenActivity.this.finish();
                break;

            case R.id.stundenplan_bearbeiten_btn_loaddatas:
                loadDatas();
                break;

            case R.id.stundenplan_bearbeiten_et_von:
                timePickerDialog = new TimePickerDialog(StundenplanBearbeitenActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar timeCalander = Calendar.getInstance();
                        timeCalander.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        timeCalander.set(Calendar.MINUTE, minute);

                        String timestring = DateUtils.formatDateTime(getApplicationContext(), timeCalander.getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME);
                        et_von.setText(timestring);
                    }
                },calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
                timePickerDialog.show();
                break;

            case R.id.stundenplan_bearbeiten_et_bis:
                timePickerDialog = new TimePickerDialog(StundenplanBearbeitenActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar timeCalander = Calendar.getInstance();
                        timeCalander.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        timeCalander.set(Calendar.MINUTE, minute);

                        String timestring = DateUtils.formatDateTime(getApplicationContext(), timeCalander.getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME);
                        et_bis.setText(timestring);
                    }
                },calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
                timePickerDialog.show();
                break;

            case R.id.stundenplan_bearbeiten_btn_cancel:
                StundenplanBearbeitenActivity.this.finish();
                break;

            default:
                Toast.makeText(this, "Fehler", Toast.LENGTH_SHORT).show();
                break;
        }

    }
}
