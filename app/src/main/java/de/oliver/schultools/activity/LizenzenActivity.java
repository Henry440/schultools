package de.oliver.schultools.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.oss.licenses.OssLicensesActivity;
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity;


import de.oliver.schultools.R;

public class LizenzenActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OssLicensesMenuActivity.setActivityTitle("Lizenzen");
        startActivity(new Intent(this, OssLicensesMenuActivity.class));
    }


}
