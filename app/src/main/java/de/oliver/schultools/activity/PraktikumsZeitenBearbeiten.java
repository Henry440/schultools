package de.oliver.schultools.activity;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.database.PraktikumTimesDB;
import de.oliver.schultools.models.PraktikumszeitenModel;

public class PraktikumsZeitenBearbeiten extends AppCompatActivity {

    Button btn_del, btn_save, btn_break;
    Toolbar toolbar;
    Spinner spinner;
    EditText et_work, et_from, et_to;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int pos = 0;
        boolean new_entry = false;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_praktikums_zeiten_bearbeiten);

        initComponents();

        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            int position = Integer.parseInt(intent.getStringExtra("position"));
            int newEntry = Integer.parseInt(intent.getStringExtra("newEntry"));

            pos = position;

            if(newEntry == 1){
                new_entry = true;
            }else{
                new_entry = false;
            }
        }

        loadConfigs(pos, new_entry);
    }

    private void initComponents() {
        toolbar = (Toolbar)findViewById(R.id.pr_bear_toolbar);
        setSupportActionBar(toolbar);

        spinner = (Spinner)findViewById(R.id.pr_bear_pos_sp);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.stundenplanspinnerstunde, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        et_work = (EditText)findViewById(R.id.pr_bear_work);
        et_from = (EditText)findViewById(R.id.pr_bear_from);
        et_to   = (EditText)findViewById(R.id.pr_bear_to);

        btn_del     = (Button)findViewById(R.id.pr_bear_del);
        btn_save    = (Button)findViewById(R.id.pr_bear_save);
        btn_break   = (Button)findViewById(R.id.pr_bear_break);

    }

    public void onClick(View view){
        int id = view.getId();
        int pos = spinner.getSelectedItemPosition();
        PraktikumszeitenModel current = getItemFromPos(pos);
        TimePickerDialog timePickerDialog;
        Calendar calendar = Calendar.getInstance();

        if(id == R.id.btn_stundenplan_back){
            this.finish();
        }else if(id == R.id.pr_bear_del){
            PraktikumTimesDB.getInstance(getApplicationContext()).deleteWork(current);
            this.finish();
        }else if(id == R.id.pr_bear_save){
            List<PraktikumszeitenModel> daten = new ArrayList<>();
            daten.addAll(PraktikumTimesDB.getInstance(getApplicationContext()).getAllWork());

            boolean exist = false;
            if(current != null) {
                for (PraktikumszeitenModel selected : daten) {
                    if (selected.getPosition() == current.getPosition()) {
                        exist = true;
                        break;
                    }
                }
            }else{
                exist = false;
            }

            if(exist){
                PraktikumTimesDB.getInstance(getApplicationContext()).updateWork(current);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Aktuallisiert", Toast.LENGTH_SHORT).show();
                    }
                });
                this.finish();
            }else{
                PraktikumszeitenModel new_model = new PraktikumszeitenModel();
                new_model.setName(et_work.getText().toString());
                new_model.setZeit_von(et_from.getText().toString());
                new_model.setZeit_bis(et_to.getText().toString());
                new_model.setPosition(pos);
                PraktikumTimesDB.getInstance(getApplicationContext()).creatWork(new_model);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Gespeichert", Toast.LENGTH_SHORT).show();
                    }
                });
                this.finish();
            }

        }else if(id == R.id.pr_bear_from){
            timePickerDialog = new TimePickerDialog(PraktikumsZeitenBearbeiten.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    Calendar timeCalander = Calendar.getInstance();
                    timeCalander.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    timeCalander.set(Calendar.MINUTE, minute);

                    String timestring = DateUtils.formatDateTime(getApplicationContext(), timeCalander.getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME);
                    et_from.setText(timestring);
                }
            },calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
            timePickerDialog.show();
        }else if(id == R.id.pr_bear_to){
            timePickerDialog = new TimePickerDialog(PraktikumsZeitenBearbeiten.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    Calendar timeCalander = Calendar.getInstance();
                    timeCalander.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    timeCalander.set(Calendar.MINUTE, minute);

                    String timestring = DateUtils.formatDateTime(getApplicationContext(), timeCalander.getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME);
                    et_to.setText(timestring);
                }
            },calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
            timePickerDialog.show();
        }
    }

    private void loadConfigs(int pos, boolean new_entry) {
        if(!new_entry) {
            PraktikumszeitenModel selected = getItemFromPos(pos);
            if(selected != null) {
                spinner.setSelection(pos);
                et_work.setText(selected.getName());
                et_from.setText(selected.getZeit_von());
                et_to.setText(selected.getZeit_bis());
            }else{
                Toast.makeText(getApplicationContext(), "Es ist ein Fehler aufgetreten", Toast.LENGTH_SHORT).show();
            }

        }else{
            spinner.setSelection(0);
        }
    }

    private PraktikumszeitenModel getItemFromPos(int pos){
        List<PraktikumszeitenModel> daten = new ArrayList<>();
        daten.addAll(PraktikumTimesDB.getInstance(getApplicationContext()).getAllWork());

        PraktikumszeitenModel selected = null;

        for (PraktikumszeitenModel current : daten){
            if(current.getPosition() == pos){
                selected = current;
                break;
            }
        }
        return selected;
    }
}
