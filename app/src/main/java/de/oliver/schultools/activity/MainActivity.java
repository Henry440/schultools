package de.oliver.schultools.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.google.firebase.analytics.FirebaseAnalytics;

import de.oliver.schultools.R;
import de.oliver.schultools.fragments.praktikum.Praktikum_Absprachen;
import de.oliver.schultools.fragments.praktikum.Praktikum_Aufgaben;
import de.oliver.schultools.fragments.praktikum.Praktikum_Zeiten;
import de.oliver.schultools.fragments.schule.Schule_Hausaufgaben;
import de.oliver.schultools.fragments.schule.Schule_Noten;
import de.oliver.schultools.fragments.schule.Schule_Stundenplan;
import de.oliver.schultools.fragments.schule.Schule_Vertretungsplan;
import de.oliver.schultools.fragments.home.Home_Frei;
import de.oliver.schultools.fragments.home.Home_Praktikum;
import de.oliver.schultools.fragments.home.Home_Schule;
import io.fabric.sdk.android.Fabric;

import static de.oliver.schultools.functions.FirstAppStart.REQUESTCODE_EXT_STO;
import static de.oliver.schultools.functions.FirstAppStart.manageFirstAppStart;
import static de.oliver.schultools.functions.ManageSharedPreferences.saveSharedPreferences;
import static de.oliver.schultools.functions.StartServices.startServices;
import static de.oliver.schultools.var.user_vars.GeneralVars.URL_MOODLE;
import static de.oliver.schultools.var.user_vars.GeneralVars.URL_SCHULWEBSITE;
import static de.oliver.schultools.var.user_vars.GeneralVars.app_is_running;
import static de.oliver.schultools.var.user_vars.GeneralVars.isFree;
import static de.oliver.schultools.var.user_vars.GeneralVars.isSchool;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static FirebaseAnalytics mFirebaseAnalytics;

    private Home_Schule home_schule_fragment = new Home_Schule();
    private Home_Praktikum home_praktikum_fragment = new Home_Praktikum();
    private Home_Frei home_frei_fragment = new Home_Frei();

    private Schule_Stundenplan schule_stundenplan_fragment = new Schule_Stundenplan();
    private Schule_Vertretungsplan schule_vertretungsplan_fragment = new Schule_Vertretungsplan();
    private Schule_Hausaufgaben schule_hausaufgaben_fragment = new Schule_Hausaufgaben();
    private Schule_Noten schule_noten_fragment = new Schule_Noten();

    private Praktikum_Zeiten praktikum_zeiten_fragment = new Praktikum_Zeiten();
    private Praktikum_Absprachen praktikum_absprachen_fragment = new Praktikum_Absprachen();
    private Praktikum_Aufgaben praktikum_aufgaben_fragment = new Praktikum_Aufgaben();

    public static MainActivity mainActivity;


    public static Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        app_is_running = true;
        setContentView(R.layout.activity_main);
        mainActivity = this;

        manageFirstAppStart(getApplicationContext());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        StrictMode.VmPolicy.Builder smbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(smbuilder.build());

        if(findViewById(R.id.fragment_container) != null){
            if(savedInstanceState != null){
                return;
            }else{
                if(isSchool){
                    toolbar.setSubtitle("Übersicht Schule");
                    getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, home_schule_fragment).commit();
                }else if(isFree){
                    toolbar.setSubtitle("Übersicht Frei");
                    getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, home_frei_fragment).commit();
                }else{
                    toolbar.setSubtitle("Übersicht Praktikum");
                    getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, home_praktikum_fragment).commit();
                }
            }
        }
        startServices(getApplicationContext());
    }

    public static  MainActivity getInstance(){
        return mainActivity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveSharedPreferences(getApplicationContext());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent startSettingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(startSettingsIntent);
        }else if(id == R.id.action_appinfo){
            Intent startAppInfoIntent = new Intent(this, AppInfo.class);
            startActivity(startAppInfoIntent);
        }else if(id == R.id.action_parameter){
            Intent startParameterActivity = new Intent(this, ParameterActivity.class);
            startActivity(startParameterActivity);
        }

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        FragmentTransaction framtrans = getSupportFragmentManager().beginTransaction();
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_schule) {
            toolbar.setSubtitle("Übersicht Schule");
            framtrans.replace((R.id.fragment_container), home_schule_fragment);
            framtrans.addToBackStack(null);
            framtrans.commit();
        } else if (id == R.id.nav_praktikum) {
            toolbar.setSubtitle("Übersicht Praktikum");
            framtrans.replace((R.id.fragment_container), home_praktikum_fragment);
            framtrans.addToBackStack(null);
            framtrans.commit();
        } else if (id == R.id.nav_frei) {
            toolbar.setSubtitle("Übersicht Frei");
            framtrans.replace((R.id.fragment_container), home_frei_fragment);
            framtrans.addToBackStack(null);
            framtrans.commit();
        } else if (id == R.id.nav_schule_Stundenplan) {
            toolbar.setSubtitle("Stundenplan");
            framtrans.replace((R.id.fragment_container), schule_stundenplan_fragment);
            framtrans.addToBackStack(null);
            framtrans.commit();
        } else if (id == R.id.nav_schule_Vertreungsplan) {
            toolbar.setSubtitle("Vertretungsplan");
            framtrans.replace((R.id.fragment_container), schule_vertretungsplan_fragment);
            framtrans.addToBackStack(null);
            framtrans.commit();
        } else if (id == R.id.nav_schule_Hausaufgaben) {
            toolbar.setSubtitle("Hausaufgaben");
            framtrans.replace((R.id.fragment_container), schule_hausaufgaben_fragment);
            framtrans.addToBackStack(null);
            framtrans.commit();
        } else if (id == R.id.nav_schule_Noten) {
            toolbar.setSubtitle("Noten");
            framtrans.replace((R.id.fragment_container), schule_noten_fragment);
            framtrans.addToBackStack(null);
            framtrans.commit();
        } else if (id == R.id.nav_praktikum_Zeiten) {
            toolbar.setSubtitle("Arbeitszeiten");
            framtrans.replace((R.id.fragment_container), praktikum_zeiten_fragment);
            framtrans.addToBackStack(null);
            framtrans.commit();
        } else if (id == R.id.nav_praktikum_apsprachen) {
            toolbar.setSubtitle("Absprachen");
            framtrans.replace((R.id.fragment_container), praktikum_absprachen_fragment);
            framtrans.addToBackStack(null);
            framtrans.commit();
        } else if (id == R.id.nav_praktikum_aufgaben) {
            toolbar.setSubtitle("Aufgaben");
            framtrans.replace((R.id.fragment_container), praktikum_aufgaben_fragment);
            framtrans.addToBackStack(null);
            framtrans.commit();
        } else if(id == R.id.nav_andrer_browser){
            openInCustumTab(URL_SCHULWEBSITE);
        } else if(id == R.id.nav_andere_moodle){
            openInCustumTab(URL_MOODLE);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openInCustumTab(String websiteURL) {

        Uri website;
        /*if(!websiteURL.contains("https://") && websiteURL.contains("http://")){
            websiteURL = "http://"+ websiteURL;
        }*/
        website = Uri.parse(websiteURL);

        CustomTabsIntent.Builder custumTab = new CustomTabsIntent.Builder();
        custumTab.setToolbarColor(Color.parseColor("#00418f"));
        custumTab.setShowTitle(true);
        custumTab.enableUrlBarHiding();

        if(cromeIsInstalled()){
            custumTab.build().intent.setPackage("com.android.chrome");
        }
        custumTab.build().launchUrl(MainActivity.this, website);


    }

    private boolean cromeIsInstalled(){
        try{
            getPackageManager().getPackageInfo("com.android.chrome", 0);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUESTCODE_EXT_STO && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            System.out.println("RECHTE Vergeben");
        }
    }
}
