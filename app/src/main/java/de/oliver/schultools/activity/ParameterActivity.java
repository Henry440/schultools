package de.oliver.schultools.activity;

import android.app.Service;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import de.oliver.schultools.R;

import static de.oliver.schultools.functions.AlertManager.getRestCounts;
import static de.oliver.schultools.var.AlertManagerVars.*;
import static de.oliver.schultools.var.user_vars.GeneralVars.*;
import static de.oliver.schultools.var.user_vars.ServiceVars.*;
import static de.oliver.schultools.var.user_vars.StundenplanVars.*;

public class ParameterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parameter);
        TextView tv_param = (TextView)findViewById(R.id.param_tv);
        tv_param.setText(
                "Internet : " + allowUsedInternet + " - " + hasInternetConnection +"\n"
                +"App läuft : " + app_is_running + "\n"
                +"Service :" + "\n"
                +"\t-->Generell : " + ServicesRunning + "\n"
                +"\t-->VP : " + ServicesVP + "\n"
                +"\t-->HW : " + ServicesHA + "\n"
                +"Stundenplan :" + "\n"
                +"\t-->Swaptime : " + TimeToSwap + "\n"
                +"\t-->Refresh Time : " + StundenplanUpdateIntervall + "\n"
                +"Restcounts :" + "\n"
                +"\t-->VP_TD_1 : " + getRestCounts(VP_TODAY_VAR_1, getApplicationContext()) + "\n"
                +"\t-->VP_TD_2 : " + getRestCounts(VP_TODAY_VAR_2, getApplicationContext()) + "\n"
                +"\t-->VP_TD_3 : " + getRestCounts(VP_TODAY_VAR_3, getApplicationContext()) + "\n"
                +"\t-->VP_TM_1 : " + getRestCounts(VP_TOMORROW_VAR_1, getApplicationContext()) + "\n"
                +"\t-->VP_TM_2 : " + getRestCounts(VP_TOMORROW_VAR_2, getApplicationContext()) + "\n"
                +"\t-->VP_TM_3 : " + getRestCounts(VP_TOMORROW_VAR_3, getApplicationContext()) + "\n"
                +"\t-->VP_MO_1 : " + getRestCounts(VP_MONDAY_VAR_1, getApplicationContext()) + "\n"
                +"\t-->VP_MO_2 : " + getRestCounts(VP_MONDAY_VAR_2, getApplicationContext()) + "\n"
                +"\t-->VP_MO_3 : " + getRestCounts(VP_MONDAY_VAR_3, getApplicationContext()) + "\n"
                +"\t-->TEST_1 : " + getRestCounts(TEST_NV_VAR_1, getApplicationContext()) + "\n"

        );
    }
}
