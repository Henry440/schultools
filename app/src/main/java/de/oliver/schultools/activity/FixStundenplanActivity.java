package de.oliver.schultools.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.database.TimetableDB;
import de.oliver.schultools.models.StundenplanModel;
import de.oliver.schultools.models.TimeTableFixModel;

public class FixStundenplanActivity extends AppCompatActivity {

    TextView tv_fix_number;
    int max_leasson = 0;
    List<TimeTableFixModel> reference = new ArrayList<>();
    List<TimeTableFixModel> shortDatas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fix_stundenplan);
        initComponents();
        initDisplayContent();
        initShortDatas();
    }

    private void initShortDatas() {
        TimeTableFixModel tm = new TimeTableFixModel(0,"","");
        shortDatas.add(tm);
        if(reference != null){
            for(TimeTableFixModel current : reference){
                Log.d("TEST_LOG", "TRUE" + shortDatas.size());
                for(TimeTableFixModel check : shortDatas){
                    Log.d("TEST_LOG", "TRUE" + shortDatas.size());
                    if(check == current){
                        Log.d("TEST_LOG", "TRUE2");
                        int max = check.getCount();
                        shortDatas.remove(check);
                        current.setCount(max + 1);
                        shortDatas.add(current);
                    }else{
                        shortDatas.add(current);
                    }
                }
            }
        }

        if(shortDatas != null){
            for(int i = 0; i < max_leasson; i++){
                TimeTableFixModel holder = new TimeTableFixModel();
                for(TimeTableFixModel current : shortDatas){
                    Log.d("TEST_LOG", "TRUE");
                    if(holder != null){
                        if(current.getCount() > holder.getCount()){
                            holder = current;
                        }else{
                            shortDatas.remove(current);
                        }
                    }else {
                        holder = current;
                    }
                }
            }
        }

        for (TimeTableFixModel current : shortDatas){
            Log.d("TEST_LOG", "Werte : " + current.getStunde() + " : " + current.getZeit_von() + " --> " + current.getZeit_bis() + " Count : " + current.getCount());
        }

    }

    private void initDisplayContent() {
        List<StundenplanModel> datas = new ArrayList<>();

        datas = TimetableDB.getInstance(getApplicationContext()).getAllStunden();
        for(StundenplanModel current : datas){
            int lesson = current.getStunde();
            if(lesson >= max_leasson){
                max_leasson = lesson;
            }
            TimeTableFixModel newDatas = new TimeTableFixModel();
            newDatas.setStunde(lesson);
            newDatas.setZeit_von(current.getZeit_Von());
            newDatas.setZeit_bis(current.getZeit_Bis());
            reference.add(newDatas);
            tv_fix_number.setText(String.valueOf(max_leasson));
        }


    }

    private void initComponents() {
        tv_fix_number = (TextView)findViewById(R.id.fixTimeTable_tv_number);
    }
}
