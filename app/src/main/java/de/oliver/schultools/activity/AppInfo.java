package de.oliver.schultools.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.adapter.AppInfoAdapter;
import de.oliver.schultools.models.AppInfoModel;

import static de.oliver.schultools.var.AppInfoVars.APP_FUNKTIONEN;
import static de.oliver.schultools.var.AppInfoVars.APP_HINT;
import static de.oliver.schultools.var.AppInfoVars.APP_NEXT_FUNCTIONS;;
import static de.oliver.schultools.var.AppInfoVars.KONTAKT_EMAIL;
import static de.oliver.schultools.var.AppInfoVars.LAST_CHANCES;
import static de.oliver.schultools.var.AppInfoVars.PROGRAMMER;
import static de.oliver.schultools.var.AppInfoVars.PROJECT_START;
import static de.oliver.schultools.var.AppInfoVars.UPDATE_NOTES;

public class AppInfo extends AppCompatActivity {
    Toolbar AppInfoToolbar;


    List<AppInfoModel> daten = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_info);
        AppInfoToolbar = (Toolbar)findViewById(R.id.AppInfo_Toolbar);
        AppInfoToolbar.setSubtitle("App Info");

        daten.add(new AppInfoModel("Programmierer", PROGRAMMER));
        daten.add(new AppInfoModel("Version", getResources().getString(R.string.nav_header_subtitle)));
        daten.add(new AppInfoModel("Projekt Start", PROJECT_START));
        daten.add(new AppInfoModel("Letzte Änderungen", LAST_CHANCES));
        daten.add(new AppInfoModel("Hinweise", buildStringFromArray(APP_HINT)));
        daten.add(new AppInfoModel("Kontakt", buildStringFromArray(KONTAKT_EMAIL)));
        daten.add(new AppInfoModel("Funktionen", buildStringFromArray(APP_FUNKTIONEN)));
        daten.add(new AppInfoModel("Funktionen in Entwicklung", buildStringFromArray(APP_NEXT_FUNCTIONS)));
        daten.add(new AppInfoModel("Letzte Änderungen", buildStringFromArray(UPDATE_NOTES)));
        daten.add(new AppInfoModel("Open Source Lizenzen", "Klicken zum Anzeigen", 1));

        ListView listView = (ListView)findViewById(R.id.AppInfo_listview);

        listView.setAdapter(new AppInfoAdapter(this, daten));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppInfoModel model = daten.get(position);
                if(model.getActionKey() == 1){
                    Intent startLizenzen = new Intent(MainActivity.getInstance().getApplicationContext(), LizenzenActivity.class);
                    MainActivity.getInstance().startActivity(startLizenzen);
                }
            }
        });
    }

    private String buildStringFromArray(String [] daten){
        String ret = "";
        for(String current : daten){
            if(ret.equals("")){
                ret = ret + "-" + current;
            }else{
                ret = ret + "\n-" + current;
            }
        }

        return ret;
    }
}
