package de.oliver.schultools.activity;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.database.HomeworkDB;
import de.oliver.schultools.models.HausaufgabenModel;

public class HausaufgabenBearbeitenActivity extends AppCompatActivity {
    Toolbar toolbar;
    EditText et_name, et_fach, et_wo, et_aufgabe, et_bemerkung;
    Spinner spinner_names, spinner_stunde, spinner_tag;
    CheckBox cb_bewertung, cb_alarm, cb_erledigt;
    Button btn_speichern, btn_delete, btn_load;

    String[] homework_names;
    ArrayAdapter<String> spinnerAdapernName;
    HausaufgabenModel display_homework;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hausaufgaben_bearbeiten);
        initComponents();
    }

    private void initComponents() {
        toolbar = (Toolbar)findViewById(R.id.hausaufgaben_bearbeitren_toolbar);
        toolbar.setSubtitle("Hausaufgaben Bearbeiten");

        spinner_names = (Spinner) findViewById(R.id.hausaufgaben_bearbeitren_sp_names);
        spinner_stunde = (Spinner)findViewById(R.id.hausaufgaben_bearbeiten_sp_stunde);
        spinner_tag = (Spinner)findViewById(R.id.hausaufgaben_bearbeiten_sp_tag);

        et_name = (EditText)findViewById(R.id.hausaufgaben_bearbeiten_et_name);
        et_fach = (EditText)findViewById(R.id.hausaufgaben_bearbeiten_et_fach);
        et_wo = (EditText)findViewById(R.id.hausaufgaben_bearbeiten_et_wo);
        et_aufgabe = (EditText)findViewById(R.id.hausaufgaben_bearbeiten_et_aufgabe);
        et_bemerkung = (EditText)findViewById(R.id.hausaufgaben_bearbeiten_et_bemerkung);

        cb_bewertung = (CheckBox) findViewById(R.id.hausaufgaben_bearbeiten_cb_Bewertung);
        cb_alarm = (CheckBox) findViewById(R.id.hausaufgaben_bearbeiten_cb_erinnerung);
        cb_erledigt = (CheckBox) findViewById(R.id.hausaufgaben_bearbeiten_cb_erledigt);

        btn_speichern = (Button)findViewById(R.id.hausaufgaben_bearbeiten_btn_speichern);
        btn_delete = (Button)findViewById(R.id.hausaufgaben_bearbeiten_btn_delete);
        btn_load = (Button)findViewById(R.id.hausaufgaben_bearbeiten_btn_load);

        ArrayAdapter<CharSequence> adapter_tage = ArrayAdapter.createFromResource(this, R.array.stundenplanspinnerdays, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter_stunde = ArrayAdapter.createFromResource(this, R.array.stundenplanspinnerstunde, android.R.layout.simple_spinner_item);

        adapter_tage.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter_stunde.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner_stunde.setAdapter(adapter_stunde);
        spinner_tag.setAdapter(adapter_tage);

        loadNamesDatasfromDB();
        spinnerAdapernName = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, homework_names);
        spinnerAdapernName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_names.setAdapter(spinnerAdapernName);
        setDisplayDatas();
    }

    private void setDisplayDatas() {
        loadNamesDatasfromDB();
        List<HausaufgabenModel> daten = new ArrayList<>();
        daten.addAll(HomeworkDB.getInstance(getApplicationContext()).getAllHomework());

        int pos = spinner_names.getSelectedItemPosition();

        if(!homework_names[pos].equals("Neuer Eintrag")){
            for(HausaufgabenModel current : daten){
                if(current.getHausaufagenName().equals(homework_names[pos])){
                    et_name.setText(current.getHausaufagenName());
                    et_fach.setText(current.getFach());
                    spinner_stunde.setSelection(current.getStunde());
                    spinner_tag.setSelection(current.getTag());
                    et_wo.setText(current.getWo());
                    et_aufgabe.setText(current.getAufgabe());

                    if(current.getBewertung() == 1){
                        cb_bewertung.setChecked(true);
                    }else{
                        cb_bewertung.setChecked(false);
                    }

                    if(current.getErledigt() == 1){
                        cb_erledigt.setChecked(true);
                    }else{
                        cb_erledigt.setChecked(false);
                    }

                    if(current.getAlarm() == 1){
                        cb_alarm.setChecked(true);
                    }else{
                        cb_alarm.setChecked(false);
                    }

                    et_bemerkung.setText(current.getBemerkung());
                    display_homework = current;

                }
            }
        }else{
            et_name.setText("");
            et_fach.setText("");
            spinner_stunde.setSelection(1);
            spinner_tag.setSelection(1);
            et_wo.setText("");
            et_aufgabe.setText("");
            cb_bewertung.setChecked(false);
            cb_erledigt.setChecked(false);
            cb_alarm.setChecked(true);
            et_bemerkung.setText("");

            display_homework = null;
        }

    }

    private void loadNamesDatasfromDB() {
        List<HausaufgabenModel> daten = new ArrayList<>();
        List<String> namen = new ArrayList<>();
        int count = 0;

        daten.addAll(HomeworkDB.getInstance(getApplicationContext()).getAllHomework());
        for(HausaufgabenModel current : daten){
            count++;
            namen.add(current.getHausaufagenName());
        }

        homework_names = new String[count + 1];
        for(int i = 0; i < count; i++){
            homework_names[i] = namen.get(i);
        }

        homework_names[count] = "Neuer Eintrag";

    }



    public void onClick(View view){
        switch (view.getId()){
            case R.id.hausaufgaben_bearbeiten_btn_delete:
                deleteHomework();
                break;
            case R.id.hausaufgaben_bearbeiten_btn_speichern:
                saveHomework();
                break;
            case R.id.hausaufgaben_bearbeiten_btn_load:
                setDisplayDatas();
                break;
            default:
                break;
        }
    }

    private void saveHomework() {
        String saved_name = spinner_names.getSelectedItem().toString();
        String new_name = String.valueOf(et_name.getText());

        boolean same_name = saved_name.equals(new_name);
        boolean saved_name_in_db = false;

        long current_id = 0;

        HausaufgabenModel displayed = null;
        HausaufgabenModel writeObj = null;

        for(HausaufgabenModel current : HomeworkDB.getInstance(getApplicationContext()).getAllHomework()){
            if(current.getHausaufagenName().equals(saved_name)){
                saved_name_in_db = true;
                displayed = current;
                current_id = displayed.getID();
                break;
            }
        }

        if(saved_name_in_db){
            writeObj = makeHausaufgabe();
            writeObj.setID(current_id);
            HomeworkDB.getInstance(getApplicationContext()).updateHomework(writeObj);
            Toast.makeText(getApplicationContext(), "Aktualisiert", Toast.LENGTH_SHORT).show();
        }else{
            writeObj = makeHausaufgabe();
            HomeworkDB.getInstance(getApplicationContext()).createHomework(writeObj);
            Toast.makeText(getApplicationContext(), "Neu", Toast.LENGTH_SHORT).show();
        }
        HausaufgabenBearbeitenActivity.this.finish();

    }

    private HausaufgabenModel makeHausaufgabe() {
        String name = String.valueOf(et_name.getText());
        String fach = String.valueOf(et_fach.getText());
        int stunde = spinner_stunde.getSelectedItemPosition();
        int tag = spinner_tag.getSelectedItemPosition();
        String wo = String.valueOf(et_wo.getText());
        String aufgabe = String.valueOf(et_aufgabe.getText());
        String bemerkung = String.valueOf(et_bemerkung.getText());

        int alarm;
        int erledigt;
        int bewertung;

        if(cb_alarm.isChecked()){
            alarm = 1;
        }else{
            alarm = 0;
        }

        if(cb_erledigt.isChecked()){
            erledigt = 1;
        }else{
            erledigt = 0;
        }

        if(cb_bewertung.isChecked()){
            bewertung = 1;
        }else{
            bewertung = 0;
        }
        HausaufgabenModel ret = new HausaufgabenModel(name, fach, stunde, tag, wo, aufgabe, bewertung, alarm, erledigt, bemerkung);
        return ret;
    }

    private void deleteHomework() {
        HomeworkDB.getInstance(getApplicationContext()).deleteHomework(display_homework);
        Toast.makeText(getApplicationContext(), "Erfolgreich Gelöscht", Toast.LENGTH_SHORT).show();
    }

}
