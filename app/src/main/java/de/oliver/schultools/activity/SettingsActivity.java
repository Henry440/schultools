package de.oliver.schultools.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import de.oliver.schultools.R;
import de.oliver.schultools.database.TimetableDB;

import static de.oliver.schultools.functions.FirstAppStart.doFirstStartAction;
import static de.oliver.schultools.functions.ManageSharedPreferences.saveSharedPreferences;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationAll;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationHomeWorkAlert;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationLessonStart;
import static de.oliver.schultools.var.user_vars.GeneralVars.getNotificationVP;
import static de.oliver.schultools.var.user_vars.GeneralVars.isFree;
import static de.oliver.schultools.var.user_vars.GeneralVars.isSchool;

public class SettingsActivity extends AppCompatActivity {
    Toolbar toolbar;
    CheckBox cb_schule, cb_praktikum, cb_frei;
    CheckBox cb_nf_all, cb_nf_vp, cb_nf_ls, cb_nf_hw;
    Button btn_update, btn_sp_del, btn_sp_fix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initComponents();
        loadStartDatas();
        //TODO deactivate/activate Background Services
        //TODO BackgroundService Intervall
    }

    private void loadStartDatas() {
        if(isSchool){
            setDisplayStartMit(0);
        }else if(isFree){
            setDisplayStartMit(2);
        }else{
            setDisplayStartMit(1);
        }
        System.out.println("YES - " + getNotificationAll);
        if(getNotificationAll){
            setDisplayNotification(0);
        }else{
            if(getNotificationVP){
                setDisplayNotification(1);
            }
            if(getNotificationLessonStart){
                setDisplayNotification(2);
            }
            if(getNotificationHomeWorkAlert){
                setDisplayNotification(3);
            }
        }
    }

    private void initComponents() {
        toolbar = (Toolbar)findViewById(R.id.settings_tolbar);
        toolbar.setSubtitle("Einstellungen");

        cb_schule = (CheckBox)findViewById(R.id.settings_cb_schule);
        cb_praktikum = (CheckBox)findViewById(R.id.settings_cb_praktikum);
        cb_frei = (CheckBox)findViewById(R.id.settings_cb_frei);

        cb_nf_all = (CheckBox)findViewById(R.id.settings_cb_nf_all);
        cb_nf_vp = (CheckBox)findViewById(R.id.settings_cb_nf_vp);
        cb_nf_ls = (CheckBox)findViewById(R.id.settings_cb_nf_ls);
        cb_nf_hw = (CheckBox)findViewById(R.id.settings_cb_nf_hw);

        btn_update = (Button)findViewById(R.id.settings_btn_update);
        btn_sp_del = (Button)findViewById(R.id.settings_btn_del);
        btn_sp_fix = (Button)findViewById(R.id.settings_btn_fix);
    }

    private void setDisplayNotification(int anzeige){
        if(anzeige == 0){
            cb_nf_all.setChecked(true);
            cb_nf_vp.setChecked(true);
            cb_nf_ls.setChecked(true);
            cb_nf_hw.setChecked(true);
        }else{
            if(anzeige == 1){
                cb_nf_vp.setChecked(true);
            }
            if(anzeige == 2){
                cb_nf_ls.setChecked(true);
            }
            if(anzeige == 3){
                cb_nf_hw.setChecked(true);
            }
        }
    }

    private void setNotificationVars() {
        System.out.println("YES2 - " + getNotificationAll);
        if(cb_nf_all.isChecked()){
            getNotificationAll = true;
            getNotificationVP = true;
            getNotificationLessonStart = true;
            getNotificationHomeWorkAlert = true;
        }else{
            getNotificationAll = false;

            if(cb_nf_vp.isChecked()){
                getNotificationVP = true;
            }else{
                getNotificationVP = false;
            }

            if(cb_nf_ls.isChecked()){
                getNotificationLessonStart = true;
            }else{
                getNotificationLessonStart = false;
            }

            if(cb_nf_hw.isChecked()){
                getNotificationHomeWorkAlert = true;
            }else{
                getNotificationHomeWorkAlert = false;
            }

            if(cb_nf_vp.isChecked()){
                if(cb_nf_ls.isChecked()){
                    if(cb_nf_hw.isChecked()){
                        cb_nf_all.setChecked(true);
                        getNotificationAll = true;
                    }
                }
            }

        }
        System.out.println("YES3 - " + getNotificationAll);
    }

    private void setDisplayStartMit(int anzeige){
        if(anzeige == 0){
            cb_schule.setChecked(true);
            cb_praktikum.setChecked(false);
            cb_frei.setChecked(false);
        }else if(anzeige == 1){
            cb_schule.setChecked(false);
            cb_praktikum.setChecked(true);
            cb_frei.setChecked(false);
        }else if(anzeige == 2){
            cb_schule.setChecked(false);
            cb_praktikum.setChecked(false);
            cb_frei.setChecked(true);
        }
    }

    private void setSchoolVars(){
        if(cb_schule.isChecked()){
            isSchool = true;
            isFree = false;
        }else if(cb_praktikum.isChecked()){
            isSchool = false;
            isFree = false;
        }else if(cb_frei.isChecked()){
            isSchool = false;
            isFree = true;
        }else{
            isSchool = true;
            isFree = false;
        }
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.settings_cb_schule:
                setDisplayStartMit(0);
                break;

            case R.id.settings_cb_praktikum:
                setDisplayStartMit(1);
                break;

            case R.id.settings_cb_frei:
                setDisplayStartMit(2);
                break;


            case R.id.settings_cb_nf_all:
                if(cb_nf_all.isChecked()){
                    setDisplayNotification(0);
                }else{
                    cb_nf_vp.setChecked(false);
                    cb_nf_ls.setChecked(false);
                    cb_nf_hw.setChecked(false);
                }
                break;

            case R.id.settings_cb_nf_vp:
                if(cb_nf_vp.isChecked()){
                    setDisplayNotification(1);
                }else{
                    cb_nf_all.setChecked(false);
                }
                break;

            case R.id.settings_cb_nf_ls:
                if(cb_nf_ls.isChecked()){
                    setDisplayNotification(2);
                }else{
                    cb_nf_all.setChecked(false);
                }
                break;

            case R.id.settings_cb_nf_hw:
                if(cb_nf_hw.isChecked()){
                    setDisplayNotification(3);
                }else{
                    cb_nf_all.setChecked(false);
                }
                break;

            case R.id.settings_btn_update:
                doFirstStartAction(getBaseContext());
                break;

            case R.id.settings_btn_del:
                delTimeTable();
                break;

            case R.id.settings_btn_fix:
                fixTimeTable();
                break;
        }

        setNotificationVars();
        setSchoolVars();
        saveSharedPreferences(getApplicationContext());
    }

    private void delTimeTable(){
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(this);
        alertdialog.setTitle("Wirklich Löschen");
        alertdialog.setMessage("Wollen sie wirklich den kompletten Stundenplan löschen");
        alertdialog.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                TimetableDB.getInstance(getApplicationContext()).deletaAllStunden();
                Toast.makeText(getApplicationContext(), "Erfolgreich Gelöscht", Toast.LENGTH_SHORT).show();
            }
        });

        alertdialog.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "Abgebrochen", Toast.LENGTH_SHORT).show();
            }
        });
        alertdialog.setCancelable(false);
        alertdialog.create().show();
    }

    private void fixTimeTable(){
        //Intent fixStundenplanIntent = new Intent(getApplicationContext(), FixStundenplanActivity.class);
        //getApplicationContext().startActivity(fixStundenplanIntent);
        Toast.makeText(getApplicationContext(), "Nicht Implementiert oder Deaktiviert", Toast.LENGTH_SHORT).show();
    }


}
