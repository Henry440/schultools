package de.oliver.schultools.fragments.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.models.HausaufgabenModel;
import de.oliver.schultools.models.StundenplanModel;

import static de.oliver.schultools.fragments.schule.Schule_Hausaufgaben.daten_hausaufgaben;
import static de.oliver.schultools.fragments.schule.Schule_Hausaufgaben.ladeHausaufgaben;
import static de.oliver.schultools.fragments.schule.Schule_Stundenplan.loadDatas;
import static de.oliver.schultools.fragments.schule.Schule_Stundenplan.getDatasFromDB;
import static de.oliver.schultools.fragments.schule.Schule_Stundenplan.setActuallyDate;
import static de.oliver.schultools.fragments.schule.Schule_Stundenplan.used_daten_stundenplan;
import static de.oliver.schultools.functions.DateAndTimes.String_to_Date;
import static de.oliver.schultools.functions.DateAndTimes.get_day_as_numer;
import static de.oliver.schultools.functions.DateAndTimes.get_hour_and_minutes_from_day;

public class Home_Schule extends Fragment {

    TextView raum_aktuell, fach_aktuell, verbleibend, anzeige, com_raum, com_raum_anz, com_fach, com_fach_anz, com_ver, com_ver_anz, tv_ha;
    List<StundenplanModel> daten_aktuell;
    View content;

    int update_Intervall = 1000* 2;

    boolean fragment_in_foreground = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        content = inflater.inflate(R.layout.home_schule_fragment, container, false);
        return content;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fragment_in_foreground = false;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragment_in_foreground = true;
        setActuallyDate();
        getDatasFromDB(getContext());
        initComponents();
        loadDatas();
        getActuallyLeasson();
        setHomework();
        siteUpdater();
    }
    private void initComponents() {
        raum_aktuell = (TextView)content.findViewById(R.id.home_schule_raumAktuell);
        fach_aktuell = (TextView)content.findViewById(R.id.home_schule_stundeAktuell);
        verbleibend = (TextView)content.findViewById(R.id.home_schule_verbleibend);
        anzeige = (TextView)content.findViewById(R.id.home_schule_anzeige3);

        com_raum = (TextView)content.findViewById(R.id.home_schule_com);
        com_raum_anz = (TextView)content.findViewById(R.id.home_schule_com_anz);
        com_fach = (TextView)content.findViewById(R.id.home_schule_com_raum);
        com_fach_anz = (TextView)content.findViewById(R.id.home_schule_com_raum_anz);
        com_ver = (TextView)content.findViewById(R.id.home_schule_com_verb);
        com_ver_anz = (TextView)content.findViewById(R.id.home_schule_com_verb_anz);
        tv_ha = (TextView)content.findViewById(R.id.home_schule_tv_ha);

        daten_aktuell = new ArrayList<StundenplanModel>();
    }

    private void getActuallyLeasson(){
        boolean sucsess = false;
        boolean firstRun = true;
        loadDatas();

        Date currentObjFrom;
        Date currentObjTo;
        Date oldcurrentObjTo = null;
        Date local;

        if(get_day_as_numer() < 6){
            //Loop durch alle Stunden des Tages bis Treffer
            for(StundenplanModel obj : used_daten_stundenplan){
                if(sucsess){//Wenn Treffer setze Anzeige und beende Schleife
                    inForeground(true);
                    break;
                }
                //Gib mir die Dates von Strings
                currentObjFrom = String_to_Date("HH:mm", obj.getZeit_Von());
                currentObjTo = String_to_Date("HH:mm", obj.getZeit_Bis());
                local = get_hour_and_minutes_from_day();

                //Erster Loop inizialisierung old time
                if(firstRun){
                    firstRun = false;
                    oldcurrentObjTo = currentObjTo;

                }
                //Findet Aktuelle stunde
                if(currentObjFrom.getTime() <= local.getTime()){
                    if(currentObjTo.getTime() > local.getTime()){
                        raum_aktuell.setText(obj.getRaum());
                        fach_aktuell.setText(obj.getFach());
                        long dauer = ((currentObjTo.getTime() - local.getTime()) / 1000 / 60 );
                        String ending = " Minute";
                        if(dauer > 1){
                            ending = " Minuten";
                        }
                        verbleibend.setText(String.valueOf(dauer) + ending);

                        int aktuelle_stunde = obj.getStunde();

                        for(StundenplanModel next : used_daten_stundenplan){
                            boolean found = false;
                            if(aktuelle_stunde < next.getStunde()){
                                found = true;
                                Date nextLeassonBeginn = String_to_Date("HH:mm", next.getZeit_Von());
                                String nexterRaum = next.getRaum();

                                dauer = (nextLeassonBeginn.getTime() - local.getTime()) / 1000 / 60;

                                ending = " Minute";
                                if(dauer > 1){
                                    ending = " Minuten";
                                }if(dauer < 0){
                                    verbleibend.setText("Feheler");
                                    break;
                                }

                                com_ver_anz.setText(String.valueOf(dauer) + ending);
                                com_fach_anz.setText(nexterRaum);
                                com_raum_anz.setText(next.getFach());
                            }

                            if (found){
                                break;
                            }else{
                                com_fach_anz.setText("Letzte Stunde");
                                com_raum_anz.setText("Letzte Stunde");
                                com_ver_anz.setText("Letzte Stunde");
                            }
                        }

                        sucsess = true;
                    }
                }
                //Findet Pause
                else if(oldcurrentObjTo.getTime() <= currentObjFrom.getTime()){
                    if(oldcurrentObjTo.getTime() > local.getTime()){
                        break;
                    }

                    raum_aktuell.setText("Pause");
                    fach_aktuell.setText("Pause");

                    inForeground(true);
                    long dauer = currentObjFrom.getTime() - local.getTime();
                    String ending = " Minute";
                    if(dauer > 1){
                        ending = " Minuten";
                    }if(dauer < 0){
                        verbleibend.setText("Feheler");
                        break;
                    }

                    verbleibend.setText(String.valueOf(dauer  / 1000 / 60) + ending);

                    int aktuelle_stunde = obj.getStunde() - 1;

                    for(StundenplanModel next : used_daten_stundenplan){
                        boolean found = false;
                        if(aktuelle_stunde < next.getStunde()){
                            found = true;
                            Date nextLeassonBeginn = String_to_Date("HH:mm", next.getZeit_Von());
                            String nexterRaum = next.getRaum();

                            dauer = (nextLeassonBeginn.getTime() - local.getTime()) / 1000 / 60;

                            ending = " Minute";
                            if(dauer > 1){
                                ending = " Minuten";
                            }if(dauer < 0){
                                verbleibend.setText("Feheler");
                                break;
                            }

                            com_ver_anz.setText(String.valueOf(dauer) + ending);
                            com_fach_anz.setText(nexterRaum);
                            com_raum_anz.setText(next.getFach());
                        }

                        if (found){
                            break;
                        }else{
                            com_fach_anz.setText("Letzte Stunde");
                            com_raum_anz.setText("Letzte Stunde");
                            com_ver_anz.setText("Letzte Stunde");
                        }
                    }


                    sucsess = true;
                }
                oldcurrentObjTo = currentObjTo;
            }
        }

        if(!sucsess){
            raum_aktuell.setText("Kein Unterricht");
            fach_aktuell.setText("Kein Unterricht");
            inForeground(false);
        }

    }

    private void inForeground(final boolean sicht){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(sicht){
                    anzeige.setVisibility(View.VISIBLE);
                    verbleibend.setVisibility(View.VISIBLE);

                    com_fach.setVisibility(View.VISIBLE);
                    com_fach_anz.setVisibility(View.VISIBLE);
                    com_raum.setVisibility(View.VISIBLE);
                    com_raum_anz.setVisibility(View.VISIBLE);
                    com_ver.setVisibility(View.VISIBLE);
                    com_ver_anz.setVisibility(View.VISIBLE);
                }else{
                    anzeige.setVisibility(View.GONE);
                    verbleibend.setVisibility(View.GONE);

                    com_fach.setVisibility(View.GONE);
                    com_fach_anz.setVisibility(View.GONE);
                    com_raum.setVisibility(View.GONE);
                    com_raum_anz.setVisibility(View.GONE);
                    com_ver.setVisibility(View.GONE);
                    com_ver_anz.setVisibility(View.GONE);
                }
            }
        });
    }

    private void siteUpdater() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadDatas();
                        ladeHausaufgaben(getContext());
                        getActuallyLeasson();
                        setHomework();
                    }
                });
                try {
                    Thread.sleep(update_Intervall);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(fragment_in_foreground){
                    siteUpdater();
                }
            }
        });
        t.start();
    }

    private void setHomework() {
        List<HausaufgabenModel> ha_daten = new ArrayList<>();
        ha_daten = daten_hausaufgaben;
        tv_ha.setText("");
        for(HausaufgabenModel current : ha_daten){
            if(current.getTag() == get_day_as_numer()){
                String erledigt = "Nein";
                if(current.getErledigt() == 1){
                    erledigt = "Ja";
                }
                tv_ha.setText(
                        "Hausaufgabe : " + current.getHausaufagenName() + "\n" +
                        "Aufgabe : " + current.getAufgabe() + "\n" +
                        "Wo : " + current.getWo() + "\n" +
                        "Fach : " + current.getFach() + "\n" +
                        "Stunde : " + current.getStunde() +  "\n" +
                        "Erledigt : " + erledigt + "\n" +
                        "" + "\n" + tv_ha.getText());
            }
        }
        if(tv_ha.getText().toString().equals("")){
            tv_ha.setText("Keine Hausaufgaben");
        }
    }
}
