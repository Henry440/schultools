package de.oliver.schultools.fragments.schule;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.activity.HausaufgabenBearbeitenActivity;
import de.oliver.schultools.adapter.HausaufgabenAdapter;
import de.oliver.schultools.database.HomeworkDB;
import de.oliver.schultools.models.HausaufgabenModel;

public class Schule_Hausaufgaben extends Fragment implements View.OnClickListener{

    ListView listView;
    Button btn_bearbeiten;

    public static List<HausaufgabenModel> daten_hausaufgaben = new ArrayList<>();

    private HausaufgabenAdapter hausaufgabenAdapter;

    View content;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        content = inflater.inflate(R.layout.schule_hausaufgaben_fragment, container, false);
        initComponents();

        btn_bearbeiten.setOnClickListener(this);
        return content;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ladeHausaufgaben(getContext());
        setzteDaten();
        hausaufgabenAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        ladeHausaufgaben(getContext());
        setzteDaten();
        hausaufgabenAdapter.notifyDataSetChanged();
    }

    private void setzteDaten() {
        listView.setAdapter(hausaufgabenAdapter);
    }

    private void initComponents() {
        listView = (ListView)content.findViewById(R.id.hausaufgaben_listview);
        btn_bearbeiten = (Button)content.findViewById(R.id.hausaufgeben_btn_bearbeiten);

        hausaufgabenAdapter = new HausaufgabenAdapter(getContext(), daten_hausaufgaben);

    }

    public static void ladeHausaufgaben(Context c) {
        daten_hausaufgaben.clear();
        daten_hausaufgaben.addAll(HomeworkDB.getInstance(c).getAllHomework());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.hausaufgeben_btn_bearbeiten:
                Intent hausaufgabenBearbeitenIntent = new Intent(getActivity(), HausaufgabenBearbeitenActivity.class);
                startActivity(hausaufgabenBearbeitenIntent);
                break;

            default:
                Toast.makeText(getContext(), "Nicht Implementiert", Toast.LENGTH_SHORT).show();
        }
    }
}
