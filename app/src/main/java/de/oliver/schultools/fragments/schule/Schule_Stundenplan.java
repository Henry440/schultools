package de.oliver.schultools.fragments.schule;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.activity.MainActivity;
import de.oliver.schultools.activity.StundenplanBearbeitenActivity;
import de.oliver.schultools.adapter.StundenplanAdapter;
import de.oliver.schultools.database.TimetableDB;
import de.oliver.schultools.models.StundenplanModel;

import static de.oliver.schultools.activity.MainActivity.toolbar;
import static de.oliver.schultools.functions.DateAndTimes.get_day_as_numer;
import static de.oliver.schultools.functions.DateAndTimes.get_hour_from_day;
import static de.oliver.schultools.var.user_vars.StundenplanVars.StundenplanUpdateIntervall;
import static de.oliver.schultools.var.user_vars.StundenplanVars.TimeToSwap;

public class Schule_Stundenplan extends Fragment implements View.OnClickListener {

    Button btn_back, btn_edit, btn_next;
    FloatingActionButton floatingActionButton;

    private static int day = 0;
    private String[] day_as_word = {"Montag", "Dienstag","Mittwoch","Donnerstag","Freitag"};

    public static List<StundenplanModel> daten_stundenplan = new ArrayList<>();
    public static List<StundenplanModel> used_daten_stundenplan = new ArrayList<>();

    static ListView listViewStundenplan;
    static StundenplanAdapter stundenplanAdapter;

    boolean fragment_in_foreground;

    View content;

    //Start Funktion Inizial
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        content = inflater.inflate(R.layout.schule_stundenplan_fragment, container, false);
        initComponents();

        btn_back.setOnClickListener(this);
        btn_edit.setOnClickListener(this);
        btn_next.setOnClickListener(this);
        floatingActionButton.setOnClickListener(this);

        return content;
    }
    private void initComponents() { //Inizialisiert die Komponenten die Verwendet werden
        btn_back = (Button)content.findViewById(R.id.btn_stundenplan_back);
        btn_edit = (Button)content.findViewById(R.id.btn_stundenplan_edit);
        btn_next = (Button)content.findViewById(R.id.btn_stundenplan_next);

        floatingActionButton = (FloatingActionButton)content.findViewById(R.id.stundenplan_floting_btn);

        listViewStundenplan = (ListView)content.findViewById(R.id.stundenplanListView);
        stundenplanAdapter = new StundenplanAdapter(getContext(), used_daten_stundenplan);

    }
    //Stopt den Seitrenupdater
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fragment_in_foreground = false;
    }

    //Startfunktion Daten Laden
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragment_in_foreground = true;
        setActuallyDate();
        setSubTitle();
        siteUpdater();
        getDatasFromDB(getContext());
        loadDatas();
        setDatas(getContext());

    }

    public static void getDatasFromDB(Context c){
        daten_stundenplan.clear();
        daten_stundenplan.addAll(TimetableDB.getInstance(c).getAllStunden());
        sortDatas();
    }

    private static void sortDatas() {
        int less_stunde = 99;
        int high_stunde = -1;

        List<StundenplanModel> sortList = new ArrayList<>();

        for(StundenplanModel current : daten_stundenplan){
            if(current.getStunde() < less_stunde){
                less_stunde = current.getStunde();
            }
            if(current.getStunde() > high_stunde){
                high_stunde = current.getStunde();
            }
        }
        for (int i = less_stunde; i <= high_stunde; i++){
            for(StundenplanModel current : daten_stundenplan){
                if(current.getStunde() == i){
                    sortList.add(current);
                }
            }
        }
        daten_stundenplan.clear();
        daten_stundenplan = sortList;


    }

    //Startmethode um Aktuelles Datum zu bekommen
    public static void setActuallyDate() {
        int tag = get_day_as_numer();
        boolean nextDay = swapDayOver();

        if(nextDay){
            day = tag;
        }else{
            day = tag - 1;
        }
        correctDate();
    }
    //Korrigirt Tag um nicht auf Samstag oder Sonntag zu kommen oder gar Negative Tage zu haben
    public static void correctDate() {
        if(day > 4){
            day = 0;
        }
        if(day < 0){
            day = 4;
        }
    }
    //Setzte auf den nächsten Tag nach einer bestimmten Zeit
    //TODO Nutzer soll diese Option deaktivieren können
    public static boolean swapDayOver() {
        boolean ret = false;
        int zeit = get_hour_from_day();
        if(zeit > TimeToSwap){
            ret = true;
        }
        return ret;
    }

    //Setzt den SubTitle des Aktuellen Tages benötigt eine gesetzte Tagvariable
    private void setSubTitle(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                toolbar.setSubtitle("Stundenplan Tag: " + day_as_word[day]);
            }
        });

    }

    //Aktuallisiert die daten_stundenplan Live --> Swap day over im Betrieb
    //TODO von Nutzer Deaktivierbar machen
    private void siteUpdater() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if(!(get_day_as_numer() >= 6)){
                    setActuallyDate();
                }
                setSubTitle();
                try {
                    Thread.sleep(StundenplanUpdateIntervall);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(fragment_in_foreground){
                    siteUpdater();
                }
            }
        });
        t.start();
    }

    @Override //OnClick abfragen für die Buttons
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_stundenplan_next:
                swapDay(true);
                break;
            case R.id.btn_stundenplan_back:
                swapDay(false);
                break;
            case  R.id.btn_stundenplan_edit:
                Intent StundenplanBearbeitenIntent = new Intent(getContext(), StundenplanBearbeitenActivity.class);
                startActivity(StundenplanBearbeitenIntent);
                break;
            case R.id.stundenplan_floting_btn:
                Intent StundenplanAddIntent = new Intent(getContext(), StundenplanBearbeitenActivity.class);
                StundenplanAddIntent.putExtra("day", "0");
                StundenplanAddIntent.putExtra("lesson", "1");
                StundenplanAddIntent.putExtra("newEntry", "1");
                startActivity(StundenplanAddIntent);
                break;
            default:
                Toast.makeText(getActivity(), "Nicht Implementiert", Toast.LENGTH_SHORT).show();
        }
    }//Schaltet zum nächsten Tag und läd die Daten
    private void swapDay(boolean action){
        if(action){
            day++;
        }else{
            day--;
        }
        correctDate();
        setSubTitle();
        loadDatas();
        setDatas(getContext());
    }

    private static void setDatas(Context c) {
        listViewStundenplan.setAdapter(new StundenplanAdapter(c, used_daten_stundenplan));

        listViewStundenplan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StundenplanModel current = (StundenplanModel) parent.getItemAtPosition(position);

                Intent StundenplanAddIntent = new Intent(MainActivity.getInstance().getApplicationContext(), StundenplanBearbeitenActivity.class);
                StundenplanAddIntent.putExtra("day", String.valueOf(current.getTag()));
                StundenplanAddIntent.putExtra("lesson", String.valueOf(current.getStunde()));
                StundenplanAddIntent.putExtra("newEntry", "0");
                MainActivity.getInstance().startActivity(StundenplanAddIntent);

            }
        });
    }

    public static void loadDatas(){//Abfrage für das setzten von Daten
        used_daten_stundenplan.clear();
        for (StundenplanModel aktuell : daten_stundenplan){
            if(day == aktuell.getTag()){
                if(!used_daten_stundenplan.isEmpty()){
                    boolean error_501 = false;
                    for(StundenplanModel vorhanden : used_daten_stundenplan){
                        if(vorhanden.getStunde() == aktuell.getStunde()){
                            error_501 = true;
                            break;
                        }else{
                            error_501 = false;
                        }
                    }
                    if(!error_501){
                        used_daten_stundenplan.add(aktuell);
                    }
                }else{
                    used_daten_stundenplan.add(aktuell);
                }
            }
        }
    }

    public static void updateLVDatas(Context c){
        getDatasFromDB(c);
        loadDatas();
        setDatas(c);
        stundenplanAdapter.notifyDataSetChanged();
    }


}
