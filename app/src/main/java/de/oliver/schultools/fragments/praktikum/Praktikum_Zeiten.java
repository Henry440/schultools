package de.oliver.schultools.fragments.praktikum;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.oliver.schultools.R;
import de.oliver.schultools.activity.MainActivity;
import de.oliver.schultools.activity.PraktikumsZeitenBearbeiten;
import de.oliver.schultools.adapter.PraktikumsZeitenAdapter;
import de.oliver.schultools.database.PraktikumTimesDB;
import de.oliver.schultools.models.PraktikumszeitenModel;
import de.oliver.schultools.models.StundenplanModel;

public class Praktikum_Zeiten extends Fragment implements View.OnClickListener{

    ListView listView;
    FloatingActionButton floatingActionButton;
    List<PraktikumszeitenModel> daten = new ArrayList<>();
    PraktikumsZeitenAdapter adapter;

    View content;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        content = inflater.inflate(R.layout.praktikum_zeiten_fragment, container, false);

        initComponents();
        floatingActionButton.setOnClickListener(this);

        return content;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ladeDaten();
    }

    @Override
    public void onResume() {
        super.onResume();
        ladeDaten();
    }


    private void ladeDaten() {
        daten.clear();
        daten.addAll(PraktikumTimesDB.getInstance(getContext()).getAllWork());
        sortDatas();
        setDatas();
    }

    private void sortDatas() {
        int high = -1;
        int less = 99;

        List<PraktikumszeitenModel> sortList = new ArrayList<>();

        for(PraktikumszeitenModel current : daten){
            if(current.getPosition() < less){
                less = current.getPosition();
            }
            if(current.getPosition() > high){
                high = current.getPosition();
            }
        }
        for (int i = less; i <= high; i++){
            for(PraktikumszeitenModel current : daten){
                if(current.getPosition() == i){
                    sortList.add(current);
                }
            }
        }
        daten.clear();
        daten = sortList;
    }

    private void setDatas() {
        listView.setAdapter(new PraktikumsZeitenAdapter(getContext(), daten));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PraktikumszeitenModel current = (PraktikumszeitenModel) parent.getItemAtPosition(position);

                Intent PraktikumBearbeitenIntent = new Intent(MainActivity.getInstance().getApplicationContext(), PraktikumsZeitenBearbeiten.class);
                PraktikumBearbeitenIntent.putExtra("position", String.valueOf(current.getPosition()));
                PraktikumBearbeitenIntent.putExtra("newEntry", "0");
                MainActivity.getInstance().startActivity(PraktikumBearbeitenIntent);

            }
        });
    }

    private void initComponents() {
        floatingActionButton = (FloatingActionButton)content.findViewById(R.id.praktikumAddFB);
        listView = (ListView)content.findViewById(R.id.praktikumZeitenLV);
        adapter = new PraktikumsZeitenAdapter(getContext(), daten);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.praktikumAddFB:
                Intent WorktimesAddIntent = new Intent(getContext(), PraktikumsZeitenBearbeiten.class);
                WorktimesAddIntent.putExtra("position", "0");
                WorktimesAddIntent.putExtra("newEntry", "1");
                startActivity(WorktimesAddIntent);
                break;
            default:
                Toast.makeText(getActivity(), "Nicht Implementiert", Toast.LENGTH_SHORT).show();
        }

    }
}
