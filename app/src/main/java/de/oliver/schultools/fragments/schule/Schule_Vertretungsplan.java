package de.oliver.schultools.fragments.schule;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import de.oliver.schultools.R;
import de.oliver.schultools.activity.MainActivity;

import static de.oliver.schultools.functions.DownloadVP.checkVerfuegbar;
import static de.oliver.schultools.functions.DownloadVP.initVerfuegbarVars;
import static de.oliver.schultools.functions.DownloadVP.startDownload;
import static de.oliver.schultools.functions.DownloadVP.verfuegbar_VP;

public class Schule_Vertretungsplan extends Fragment implements  View.OnClickListener{
    View context;

    Button heute, morgen;
    boolean inForeground = false;

    String[] btn_texte = {"Heute", "Morgen", "Montag", "Nicht Verfügbar"};

    Thread loop;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = inflater.inflate(R.layout.schule_vertretungsplan_fragment, container, false);
        initComponents();
        return context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inForeground = true;
        initVerfuegbarVars();
        setButtons();
        startTask();
    }

    private void startTask() {
        loop = new Thread(new Runnable() {
            @Override
            public void run() {
                while (inForeground){
                    try{
                        t.start();

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {
                        Thread.sleep(1000 * 30 * 1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    checkVerfuegbar(getContext());
                    setButtons();
                }
            });
        });
        loop.start();
    }

    private void setButtons() {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean vpHeute = verfuegbar_VP[0];
                    boolean vpMorgen = verfuegbar_VP[1];
                    boolean vpMonatag = verfuegbar_VP[2];

                    if (vpHeute) {
                        heute.setText(btn_texte[0]);
                    } else {
                        heute.setText(btn_texte[3]);
                    }

                    if (vpMonatag) {
                        morgen.setText(btn_texte[2]);
                    } else if (vpMorgen) {
                        morgen.setText(btn_texte[1]);
                    } else {
                        morgen.setText(btn_texte[3]);
                    }
                }
            });
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        inForeground = false;
    }

    private void initComponents() {
        heute = (Button)context.findViewById(R.id.vertretungsplan_btn_heute);
        morgen = (Button)context.findViewById(R.id.vertretungsplan_btn_morgen);
        heute.setOnClickListener(this);
        morgen.setOnClickListener(this);
        setButtons();
    }

    @Override
    public void onClick(View v) {
        String btn_text;
        switch (v.getId()){
            case R.id.vertretungsplan_btn_heute:
                btn_text = (String) heute.getText();
                if(btn_text.equals(btn_texte[0])){
                    Thread st0 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            startDownload(getContext(), 0);
                        }
                    });
                    st0.start();
                }else{
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(), "Nicht Verfügbar", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                break;

            case R.id.vertretungsplan_btn_morgen:
                btn_text = (String) morgen.getText();
                if(!(btn_text.equals(btn_texte[3]))){
                    if(btn_text.equals(btn_texte[1])){
                        Thread st1 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                startDownload(getContext(), 1);
                            }
                        });
                        st1.start();
                    }else if(btn_text.equals(btn_texte[2])){
                        Thread st2 = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                startDownload(getContext(), 2);
                            }
                        });
                        st2.start();
                    }
                }else{
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(), "Nicht Verfügbar", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                break;

        }
    }
}
